#/!/bin/bash

# Used to run tests in the same image as in gitlab ci. This makes sure the screenshots
# are the same between local and ci environments.

# remove this shit once playwright improves the diffing algo (for example with blurring)

docker run  -it --rm \
  --ipc=host -u "$(id -u):$(stat -c '%g' /var/run/docker.sock)" \
  --net=host \
  -v "/var/run/docker.sock:/var/run/docker.sock" \
  -v "$(realpath $(dirname $0)/..):/workdir" \
  registry.gitlab.com/benoitaverty/pcrops/ci:latest \
  bash -c "npm run test:e2e"
