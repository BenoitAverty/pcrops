-- List users and how many crops and zones they have
select U.*, count(distinct C.tid) nCrops, count(distinct GZ.tid) nZones
from "User" U
         left join "Crop" C on U.tid = C."ownerTid"
         left join "GardenZone" GZ on U.tid = GZ."ownerTid"
group by U.tid
order by nCrops desc;

-- List last active users
select U.*, count(distinct C.tid) nCrops
from "User" U
         left join "Crop" C on U.tid = C."ownerTid"
group by U.tid
order by U."lastSeenAt" desc;

-- List crops of a given user
select C.*
from "Crop" C
where "ownerTid" = ?;

-- List zones
select GZ.* from "GardenZone" GZ where "ownerTid" = ?;

-- Select users that have done nothing
-- delete
SELECT count(*)
from "User"
where tid not in (
    select distinct "ownerTid"
    from "Crop"
    union all
    select distinct "ownerTid"
    from "GardenZone"
);

-- Count users and active users
select count(*) from "User";
select count(distinct "ownerTid") from "Crop";
