#!/bin/bash

docker build -f $(dirname $0)/ci.Dockerfile -t registry.gitlab.com/benoitaverty/pcrops/ci:latest $(dirname $0)
docker push registry.gitlab.com/benoitaverty/pcrops/ci:latest
