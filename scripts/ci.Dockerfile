FROM mcr.microsoft.com/playwright:v1.29.0-focal

# Install docker and docker compose
RUN curl -sL https://get.docker.com | bash -
RUN curl -SL https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose

# upgrade to node 18 (this also apt-get update)
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - && \
    apt-get install -y nodejs

# Easier commands
WORKDIR /workdir
