module.exports = {
  root: true,
  ignorePatterns: [
    "remix.config.js",
    ".eslintrc.js",
    "build/*",
    "public/build/*",
    "tailwind.config.js",
  ],
  parser: "@typescript-eslint/parser",
  plugins: ["only-error", "@typescript-eslint", "prettier", "react", "react-hooks"],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended",
    "plugin:import/recommended",
    "plugin:import/typescript",
  ],
  rules: {
    "react/react-in-jsx-scope": "off",
    "react/jsx-sort-props": [
      "error",
      {
        callbacksLast: true,
        reservedFirst: true,
      },
    ],
    "import/order": ["error", { "newlines-between": "always" }],
    "@typescript-eslint/no-unused-vars": [2, { argsIgnorePattern: "^_$" }],
    // Why the rule below : https://blog.variant.no/a-better-way-to-type-react-components-9a6460a1d4b7
    "react/function-component-definition": [2, { namedComponents: "function-declaration" }],
    "react/jsx-curly-brace-presence": [2, { children: "never", props: "never" }],
  },
  settings: {
    "import/resolver": {
      typescript: {
        alwaysTryTypes: true, // always try to resolve types under `<root>@types` directory even it doesn't contain any source code, like `@types/unist`
      },
    },
    react: {
      version: "detect",
    },
    formComponents: [
      // Components used as alternatives to <form> for forms, eg. <Form endpoint={ url } />
      { name: "Form", formAttribute: "action" },
    ],
    linkComponents: [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      { name: "Link", linkAttribute: "to" },
    ],
  },
  overrides: [
    { files: "**/*.spec.(j|t)sx?", globals: { jest: true } },
    { files: "playwright/**/*.spec.(j|t)sx?", env: { node: true } },
    {
      files: "opentelemetry.js",
      env: {
        node: true,
      },
    },
  ],
};
