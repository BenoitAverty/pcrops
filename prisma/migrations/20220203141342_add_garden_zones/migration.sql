-- AlterTable
ALTER TABLE "Crop" ADD COLUMN     "zoneTid" TEXT;

-- CreateTable
CREATE TABLE "GardenZone" (
    "tid" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "order" INTEGER NOT NULL,
    "ownerTid" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "GardenZone_pkey" PRIMARY KEY ("tid")
);

-- CreateIndex
CREATE UNIQUE INDEX "GardenZone_ownerTid_name_key" ON "GardenZone"("ownerTid", "name");

-- AddForeignKey
ALTER TABLE "GardenZone" ADD CONSTRAINT "GardenZone_ownerTid_fkey" FOREIGN KEY ("ownerTid") REFERENCES "User"("tid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Crop" ADD CONSTRAINT "Crop_zoneTid_fkey" FOREIGN KEY ("zoneTid") REFERENCES "GardenZone"("tid") ON DELETE SET NULL ON UPDATE CASCADE;
