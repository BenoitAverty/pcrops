-- This is an empty migration.
DROP INDEX "GardenZone_ownerTid_name_key";
CREATE UNIQUE INDEX "GardenZone_ownerTid_name_key" ON "GardenZone"("ownerTid", LOWER("name"));