-- CreateEnum
CREATE TYPE "CropState" AS ENUM ('ABSENT', 'SOWING', 'PLANTING', 'GROWTH', 'HARVEST');

-- CreateTable
CREATE TABLE "User" (
    "tid" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("tid")
);

-- CreateTable
CREATE TABLE "CropPeriod" (
    "cropTid" TEXT NOT NULL,
    "startWeek" INTEGER NOT NULL,
    "endWeek" INTEGER NOT NULL,
    "state" "CropState" NOT NULL,

    CONSTRAINT "CropPeriod_pkey" PRIMARY KEY ("cropTid","startWeek","endWeek","state")
);

-- CreateTable
CREATE TABLE "Crop" (
    "tid" TEXT NOT NULL,
    "species" TEXT NOT NULL,
    "variety" TEXT,
    "ownerTid" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Crop_pkey" PRIMARY KEY ("tid")
);

-- AddForeignKey
ALTER TABLE "CropPeriod" ADD CONSTRAINT "CropPeriod_cropTid_fkey" FOREIGN KEY ("cropTid") REFERENCES "Crop"("tid") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Crop" ADD CONSTRAINT "Crop_ownerTid_fkey" FOREIGN KEY ("ownerTid") REFERENCES "User"("tid") ON DELETE RESTRICT ON UPDATE CASCADE;