import { resolve } from "node:path";
import * as process from "process";

import { renderToString } from "react-dom/server";
import type { EntryContext } from "@remix-run/server-runtime";
import { createInstance } from "i18next";
import { I18nextProvider, initReactI18next } from "react-i18next";
import { resetServerContext } from "react-beautiful-dnd";
import Backend from "i18next-fs-backend";
import { RemixServer } from "@remix-run/react"; // Because of a bug. https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import remixI18Next from "~/lib/common/i18next.server";
import i18nConfig from "~/i18n-config";

export default async function handleRequest(
  request: Request,
  responseStatusCode: number,
  responseHeaders: Headers,
  remixContext: EntryContext,
) {
  resetServerContext();

  const i18nextInstance = createInstance();
  const lng = await remixI18Next.getLocale(request);

  await i18nextInstance
    .use(initReactI18next)
    .use(Backend)
    .init({
      ...i18nConfig,
      lng,
      ns: "common",
      saveMissing: process.env.NODE_ENV === "development",
      backend: {
        loadPath: resolve("./public/locales/{{lng}}/{{ns}}.json"),
        addPath: resolve("./public/locales/{{lng}}/{{ns}}.missing.json"),
      },
    });

  const markup = renderToString(
    <I18nextProvider i18n={i18nextInstance}>
      <RemixServer context={remixContext} url={request.url} />
    </I18nextProvider>,
  );

  responseHeaders.set("Content-Type", "text/html");

  return new Response("<!DOCTYPE html>" + markup, {
    status: responseStatusCode,
    headers: responseHeaders,
  });
}
