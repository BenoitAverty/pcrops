import { useTranslation } from "react-i18next";
import { ForwardedRef, forwardRef } from "react";

type SpeciesSelectProps = Omit<React.HTMLProps<HTMLInputElement>, "type">;

function SpeciesSelect(
  { className, ...props }: SpeciesSelectProps,
  ref: ForwardedRef<HTMLInputElement>,
) {
  const { t } = useTranslation();

  return (
    <div className={`flex flex-col grow ${className}`}>
      <input
        ref={ref}
        aria-label={t("speciesNamePlaceholder")}
        className="form-add-text-input"
        placeholder={t("speciesNamePlaceholder")}
        type="text"
        {...props}
      />
    </div>
  );
}

export default forwardRef(SpeciesSelect);
