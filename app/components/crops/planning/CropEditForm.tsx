import React, { MutableRefObject } from "react";
import { Crop } from "@prisma/client";
import invariant from "tiny-invariant";
import { clsx } from "clsx";
import { SerializeFrom } from "@remix-run/server-runtime";
import { Form } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import DeleteIcon from "~/components/common/DeleteIcon";
import { drop, update } from "~/lib/crops/datastore";

export const actions = {
  async updateCrop(formData: FormData) {
    const plan = formData.get("plan") as string;
    const partialCrop = {
      tid: formData.get("cropTid") as string,
      plan: plan ? JSON.parse(plan) : undefined,
    };
    console.debug(plan);
    await update(partialCrop);
  },
  async deleteCrop(formData: FormData) {
    const cropTid = formData.get("cropTid") as string;
    invariant(cropTid, "tid of the crop to delete must be provided");
    await drop(cropTid);
  },
};

export type CropEditFormProps = {
  crop: SerializeFrom<Crop>;
  id: string;
  disabled?: boolean;
  submitButtonRef: MutableRefObject<HTMLButtonElement | null>;
};

export default function CropEditForm({
  crop,
  id,
  submitButtonRef,
  disabled = false,
}: CropEditFormProps) {
  return (
    <Form className="group p-0 m-0 w-full min-w-0" id={id} method="post">
      <input name="cropTid" type="hidden" value={crop.tid} />
      <div className="flex font-normal truncate bg-inherit group-hover:overflow-visible group-hover:absolute">
        <div className="grow">{crop.species}</div>
        <button
          ref={submitButtonRef}
          className="hidden"
          name="actionName"
          type="submit"
          value="updateCrop"
        />
        <button
          aria-label="Delete crop"
          className={clsx("invisible ml-1", disabled || "group-hover:visible")}
          disabled={disabled}
          name="actionName"
          type="submit"
          value="deleteCrop"
        >
          <DeleteIcon />
        </button>
      </div>
    </Form>
  );
}
