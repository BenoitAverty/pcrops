import { HTMLProps, useRef, useState } from "react";

import { computeNewPeriod, OrphanCropPeriod } from "~/lib/crops/model";
import CropRowWeek from "~/components/crops/planning/CropRowWeek";
import WeekCell from "~/components/calendar/WeekCell";
import { makeWeeks } from "~/lib/calendar/calendar-utils";
import { Week } from "~/lib/calendar/weeks";
import { useCalendarPeriod } from "~/components/calendar/Calendar";

export type CropPlanInputProps = {
  defaultValue: OrphanCropPeriod[];
  onChange?: (newValue: OrphanCropPeriod[]) => void;
} & Omit<HTMLProps<HTMLInputElement>, "defaultValue" | "onChange">;

type Dragging = {
  dragStart: Week | null;
  dragAction: "resize" | "move" | null;
};

/**
 * Input that allows to edit the periods of a crop.
 *
 * This auto submits on drag end.
 *
 * Must be inside a CalendarRow body.
 */
export default function CropPlanInput({ defaultValue, onChange, ...props }: CropPlanInputProps) {
  const [dragging, setDragging] = useState<Dragging>({ dragStart: null, dragAction: null });
  const [tempValue, setTempValue] = useState(defaultValue);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const calendarPeriod = useCalendarPeriod();
  const weeks = makeWeeks(
    calendarPeriod.startingYear,
    calendarPeriod.startingMonth,
    calendarPeriod.durationInMonths,
  );

  const handlePeriodChange = (targetWeek: Week) => {
    if (dragging.dragStart && dragging.dragAction) {
      const newCrop = computeNewPeriod(
        defaultValue,
        dragging.dragAction,
        dragging.dragStart,
        targetWeek,
      );
      setTempValue(newCrop);
    }
  };
  const handleDrop = (targetWeek: Week) => {
    if (dragging.dragStart && dragging.dragAction) {
      setDragging({ dragStart: null, dragAction: null });
      const newPlan = computeNewPeriod(
        defaultValue,
        dragging.dragAction,
        dragging.dragStart,
        targetWeek,
      );
      setTempValue(newPlan);
      if (inputRef.current) {
        inputRef.current.value = JSON.stringify(newPlan);
        onChange && onChange(newPlan);
      }
    }
  };

  return (
    <>
      {weeks.map((w, i) => (
        <WeekCell key={`${w.year}-${w.number}`}>
          {i === 0 && (
            <input
              type="hidden"
              {...props}
              ref={inputRef}
              defaultValue={JSON.stringify(defaultValue)}
            />
          )}
          <CropRowWeek
            plan={tempValue}
            week={w}
            onDragOver={() => handlePeriodChange(w)}
            onDragStart={(dragAction) => setDragging({ dragStart: w, dragAction })}
            onDrop={() => handleDrop(w)}
          />
        </WeekCell>
      ))}
    </>
  );
}
