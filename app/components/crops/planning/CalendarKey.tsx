import { useTranslation } from "react-i18next";

export type CalendarKeyProps = {
  items?: ("sowing" | "planting" | "growth" | "harvest")[];
};

export default function CalendarKey({
  items = ["sowing", "planting", "growth", "harvest"],
}: CalendarKeyProps) {
  const { t } = useTranslation();
  return (
    <div className="flex m-auto w-full justify-center items-center">
      {items.includes("sowing") && (
        <div className="flex items-center justify-between mx-3">
          <div className="h-5 w-5 bg-green-500" />
          &nbsp;{t("sowing")}
        </div>
      )}
      {items.includes("planting") && (
        <div className="flex items-center justify-between mx-3">
          <div className="h-5 w-5 bg-purple-500" />
          &nbsp;{t("planting")}
        </div>
      )}
      {items.includes("growth") && (
        <div className="flex items-center justify-between mx-3">
          <div className="h-5 w-5 bg-green-200" />
          &nbsp;{t("growth")}
        </div>
      )}
      {items.includes("harvest") && (
        <div className="flex items-center justify-between mx-3">
          <div className="h-5 w-5 bg-yellow-500" />
          &nbsp;{t("harvest")}
        </div>
      )}
    </div>
  );
}
