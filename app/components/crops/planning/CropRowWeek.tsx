import { CropState } from "@prisma/client";
import { clsx } from "clsx";

import {
  currentPeriodIndex,
  OrphanCropPeriod,
  periodInclude,
  periodLength,
} from "~/lib/crops/model";
import { compareWeeks, w, Week } from "~/lib/calendar/weeks";

export type CropRowWeekProps = {
  plan: OrphanCropPeriod[];
  week: Week;
  onDragStart?: (action: "move" | "resize") => void;
  onDragOver?: () => void;
  onDrop?: () => void;
};

export default function CropRowWeek({
  plan,
  week,
  onDragStart,
  onDragOver,
  onDrop,
}: CropRowWeekProps) {
  const idx = currentPeriodIndex(plan, week);
  const currentPeriod = idx >= 0 ? plan[idx] : null;

  if (!currentPeriod) {
    return (
      <div
        className="text-white bg-transparent"
        data-testid={`week-${week.year}-${week.number}`}
        onMouseOver={onDragOver}
        onMouseUp={onDrop}
      >
        &nbsp;
      </div>
    );
  } else if (periodLength(currentPeriod) === 1) {
    return (
      <div className="flex w-full">
        <div
          className={clsx("grow [cursor:ew-resize]", computeColor(week, plan))}
          data-testid={`week-${week.year}-${week.number}-start`}
          onMouseDown={() => onDragStart && onDragStart("resize")}
          onMouseOver={onDragOver}
          onMouseUp={onDrop}
        >
          &nbsp;
        </div>
        <div
          className={clsx("grow cursor-move", computeColor(week, plan))}
          data-testid={`week-${week.year}-${week.number}-middle`}
          onMouseDown={() => onDragStart && onDragStart("move")}
          onMouseOver={onDragOver}
          onMouseUp={onDrop}
        >
          &nbsp;
        </div>
        <div
          className={clsx("grow [cursor:ew-resize]", computeColor(week, plan))}
          data-testid={`week-${week.year}-${week.number}-end`}
          onMouseDown={() => onDragStart && onDragStart("resize")}
          onMouseOver={onDragOver}
          onMouseUp={onDrop}
        >
          &nbsp;
        </div>
      </div>
    );
  } else if (periodLength(currentPeriod) === 2) {
    return (
      <div className="flex">
        <div
          className={clsx(
            "grow",
            week.number === currentPeriod.startWeek && week.year === currentPeriod.startYear
              ? "[cursor:ew-resize]"
              : "cursor-move",
            computeColor(week, plan),
          )}
          data-testid={`week-${week.year}-${week.number}-start`}
          onMouseDown={() =>
            onDragStart &&
            onDragStart(
              week.number === currentPeriod.startWeek && week.year && currentPeriod.startYear
                ? "resize"
                : "move",
            )
          }
          onMouseOver={onDragOver}
          onMouseUp={onDrop}
        >
          &nbsp;
        </div>
        <div
          className={clsx("grow", "[cursor:ew-resize]", computeColor(week, plan))}
          data-testid={`week-${week.year}-${week.number}-middle`}
          onMouseDown={() => onDragStart && onDragStart("resize")}
          onMouseOver={onDragOver}
          onMouseUp={onDrop}
        >
          &nbsp;
        </div>
        <div
          className={clsx(
            "grow",
            compareWeeks(week, w(currentPeriod.endWeek, currentPeriod.endYear)) === 0
              ? "[cursor:ew-resize]"
              : "cursor-move",
            computeColor(week, plan),
          )}
          data-testid={`week-${week.year}-${week.number}-end`}
          onMouseDown={() =>
            onDragStart &&
            onDragStart(
              compareWeeks(week, w(currentPeriod.endWeek, currentPeriod.endYear)) === 0
                ? "resize"
                : "move",
            )
          }
          onMouseOver={onDragOver}
          onMouseUp={onDrop}
        >
          &nbsp;
        </div>
      </div>
    );
  } else {
    const isStart = compareWeeks(w(currentPeriod.startWeek, currentPeriod.startYear), week) === 0;
    const isEnd = compareWeeks(w(currentPeriod.endWeek, currentPeriod.endYear), week) === 0;
    const cursor = isStart || isEnd ? "[cursor:ew-resize]" : "cursor-move";
    const action = isStart || isEnd ? "resize" : "move";
    return (
      <div
        className={clsx(cursor, computeColor(week, plan))}
        data-testid={`week-${week.year}-${week.number}`}
        onMouseDown={() => onDragStart && onDragStart(action)}
        onMouseOver={onDragOver}
        onMouseUp={onDrop}
      >
        &nbsp;
      </div>
    );
  }
}

function computeColor(week: Week, plan: OrphanCropPeriod[]) {
  const state = plan.find((period) => periodInclude(period, week))?.state;
  if (state === CropState.SOWING) {
    return "bg-green-500 text-green-500";
  } else if (state === CropState.PLANTING) {
    return "bg-purple-500 text-purple-500";
  } else if (state === CropState.HARVEST) {
    return "bg-yellow-500 text-yellow-500";
  } else if (state === CropState.GROWTH) {
    return "bg-green-200 text-green-200";
  } else return "text-white bg-transparent";
}
