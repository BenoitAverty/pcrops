import { DragDropContext, Droppable, DropResult } from "react-beautiful-dnd";
import { useTranslation } from "react-i18next";
import invariant from "tiny-invariant";
import React from "react";
import { SerializeFrom } from "@remix-run/server-runtime";
import { useSubmit } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import { GroupedCrops } from "~/lib/zones/model";
import { FullCrop } from "~/lib/crops/model";
import Calendar from "~/components/calendar/Calendar";
import RowGroup from "~/components/calendar/RowGroup";
import { actions as cropEditFormActions } from "~/components/crops/planning/CropEditForm";
import EmptyRow from "~/components/calendar/EmptyRow";
import { ComponentActions } from "~/lib/common/component-actions";
import { assignZone } from "~/lib/crops/datastore";
import DraggableCropRow from "~/components/crops/planning/DraggableCropRow";
import { CalendarPeriod } from "~/lib/calendar/calendar-utils";

export const actions: ComponentActions = {
  ...cropEditFormActions,
  async assignCrop(formData) {
    const cropTid = formData.get("cropTid");
    const targetZoneTid = formData.get("targetZoneTid");

    invariant(
      typeof cropTid === "string" && typeof targetZoneTid === "string",
      "Missing parameter to assign a zone",
    );

    await assignZone(cropTid, targetZoneTid);
  },
  async unassignCrop(formData) {
    const cropTid = formData.get("cropTid");

    invariant(typeof cropTid === "string", "Missing tid of the crop to unassign");

    await assignZone(cropTid, null);
  },
};

export type EditablePlanningProps = {
  groupedCrops: SerializeFrom<GroupedCrops<FullCrop>>;
  calendarPeriod: CalendarPeriod;
};

export default function EditablePlanning({ groupedCrops, calendarPeriod }: EditablePlanningProps) {
  const { t } = useTranslation();
  const submit = useSubmit();

  const handleDragEnd = (dropResult: DropResult) => {
    if (!dropResult.destination) return;

    // No reorder yet :(
    if (dropResult.source.droppableId === dropResult.destination.droppableId) return;

    if (dropResult.destination.droppableId === "noZone") {
      submit(
        {
          actionName: "unassignCrop",
          cropTid: dropResult.draggableId,
        },
        { method: "post" },
      );
    } else {
      submit(
        {
          actionName: "assignCrop",
          cropTid: dropResult.draggableId,
          targetZoneTid: dropResult.destination.droppableId,
        },
        { method: "post" },
      );
    }
  };

  const cropsNumber =
    groupedCrops.noZone.length +
    Object.values(groupedCrops.zones)
      .map((z) => z.crops.length)
      .reduce((a, b) => a + b, 0);
  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <Calendar
        period={calendarPeriod}
        showBottomHeading={cropsNumber > 15}
        topLeftHeading={t("speciesNameColumnHeading")}
      >
        {cropsNumber > 0 ? (
          <>
            <Droppable droppableId="noZone">
              {(provided, snapshot) => (
                <RowGroup {...provided.droppableProps} ref={provided.innerRef}>
                  {groupedCrops.noZone.map((c, i) => (
                    <DraggableCropRow
                      key={c.tid}
                      crop={c}
                      disabled={snapshot.isDraggingOver}
                      index={i}
                    />
                  ))}
                  {provided.placeholder}
                </RowGroup>
              )}
            </Droppable>
            {groupedCrops.orderedZonesKeys.map((zoneName) => (
              // We know zoneName is a key of groupedCrops.zones, but typescript can't know it. Hence the "as string".
              <Droppable
                key={zoneName}
                droppableId={groupedCrops.zones[zoneName]?.zone?.tid as string}
              >
                {(provided, snapshot) => (
                  <RowGroup
                    {...provided.droppableProps}
                    key={zoneName}
                    ref={provided.innerRef}
                    title={zoneName}
                  >
                    {groupedCrops.zones[zoneName].crops.map((c, i) => (
                      <DraggableCropRow
                        key={c.tid}
                        crop={c}
                        disabled={snapshot.isDraggingOver}
                        index={i}
                      />
                    ))}
                    {provided.placeholder}
                  </RowGroup>
                )}
              </Droppable>
            ))}
          </>
        ) : (
          <EmptyRow>{t("emptyCalendarMessage")}</EmptyRow>
        )}
      </Calendar>
    </DragDropContext>
  );
}
