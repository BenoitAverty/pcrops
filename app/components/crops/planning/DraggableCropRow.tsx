import React, { useRef } from "react";
import { Draggable } from "react-beautiful-dnd";
import { clsx } from "clsx";
import { SerializeFrom } from "@remix-run/server-runtime";
import { useSubmit } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import { PlannedCrop } from "~/lib/crops/model";
import CalendarRow from "~/components/calendar/CalendarRow";
import CropEditForm from "~/components/crops/planning/CropEditForm";
import CropPlanInput from "~/components/crops/planning/CropPlanInput";

export type DraggableCropRowProps = {
  crop: SerializeFrom<PlannedCrop>;
  disabled?: boolean;
  index: number;
};

export default function DraggableCropRow({ crop, disabled = false, index }: DraggableCropRowProps) {
  const submit = useSubmit();
  const editCropButtonRef = useRef(null);

  return (
    <Draggable draggableId={crop.tid} index={index}>
      {(provided) => (
        <CalendarRow {...provided.draggableProps} ref={provided.innerRef}>
          {{
            heading: (
              <div className="flex group">
                <span
                  {...provided.dragHandleProps}
                  className={clsx(
                    "drag-handle invisible shrink-0",
                    disabled || "group-hover:visible",
                  )}
                  data-testid="row-drag-handle"
                >
                  &nbsp;
                </span>
                <CropEditForm
                  crop={crop}
                  disabled={disabled}
                  id={`crop-${crop.tid}`}
                  submitButtonRef={editCropButtonRef}
                />
              </div>
            ),
            body: (
              <CropPlanInput
                defaultValue={crop.plan}
                form={`crop-${crop.tid}`}
                name="plan"
                onChange={() => submit(editCropButtonRef.current)}
              />
            ),
          }}
        </CalendarRow>
      )}
    </Draggable>
  );
}
