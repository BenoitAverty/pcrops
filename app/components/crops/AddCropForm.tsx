import { useTranslation } from "react-i18next";
import { User } from "@prisma/client";
import invariant from "tiny-invariant";
import { Form } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import SpeciesSelect from "./SpeciesSelect";

import { ComponentActions } from "~/lib/common/component-actions";
import { create } from "~/lib/crops/datastore";
import { defaultCrop } from "~/lib/crops/model";
import { useFormReset } from "~/components/common/utils";

export const addCropActions: (user: User) => ComponentActions = (user: User) => ({
  async addCropPlanting(formData) {
    const species = formData.get("species") as string;
    invariant(species, "species name must be provided");
    await create(defaultCrop(user.tid, species, false, true));
  },
  async addCropSowing(formData) {
    const species = formData.get("species") as string;
    invariant(species, "species name must be provided");
    await create(defaultCrop(user.tid, species, true, false));
  },
  async addCropTransplanting(formData) {
    const species = formData.get("species") as string;
    invariant(species, "species name must be provided");
    await create(defaultCrop(user.tid, species, true, true));
  },
});

type AddCropFormProps = {
  hideOptions?: boolean;
};

export default function AddCropFrom({ hideOptions = false }: AddCropFormProps) {
  const { t } = useTranslation();

  const { formRef, inputRef } = useFormReset([
    "addCropPlanting",
    "addCropSowing",
    "addCropTransplanting",
  ]);

  return (
    <Form ref={formRef} className="flex flex-col items-center m-auto w-1/2" method="post">
      <SpeciesSelect ref={inputRef} autoFocus className="self-stretch" name="species" required />
      {hideOptions ? (
        <button
          className="button-primary shrink"
          name="actionName"
          type="submit"
          value="addCropPlanting"
        >
          {t("addCropFormAdd")}
        </button>
      ) : (
        <div className="flex justify-center ">
          <button
            className="button-primary shrink"
            name="actionName"
            type="submit"
            value="addCropPlanting"
          >
            {t("addCropFormPlant")}
          </button>
          <button
            className="button-primary shrink"
            name="actionName"
            type="submit"
            value="addCropSowing"
          >
            {t("addCropFormSow")}
          </button>
          <button
            className="button-primary shrink"
            name="actionName"
            type="submit"
            value="addCropTransplanting"
          >
            {t("addCropFormTransplant")}
          </button>
        </div>
      )}
    </Form>
  );
}
