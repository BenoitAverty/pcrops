import { Trans, useTranslation } from "react-i18next";
import { NavLink } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import Commit from "./Commit";

type LayoutProps = React.PropsWithChildren<{ commit?: string }>;

export default function Layout({ commit, children }: LayoutProps) {
  const { t } = useTranslation();

  return (
    <div className="container mx-auto">
      <header className="mt-5 text-center">
        <h1>🌱 PCrops</h1>
        <nav aria-label="Main navigation" className="mt-1">
          <ul className="flex flex-row gap-x-5 justify-center">
            <li>
              <NavLink prefetch="intent" to="/plan/crops">
                {t("planLink") as string}
              </NavLink>
            </li>
            <li>
              <NavLink prefetch="intent" to="/settings">
                {t("settingsLink") as string}
              </NavLink>
            </li>
          </ul>
        </nav>
      </header>
      {children}
      <footer className="mt-5 text-center border-t border-black">
        <div>
          <p>
            <Trans i18nKey="footer" t={t}>
              PCrops is <a href="https://gitlab.com/BenoitAverty/pcrops">Open-Source</a> and
              developed by a gardening newbie.
            </Trans>
          </p>
          <Commit commit={commit} />
        </div>
      </footer>
    </div>
  );
}
