import { useTranslation } from "react-i18next";

export type CommitProps = {
  commit?: string;
};

export default function Commit({ commit }: CommitProps) {
  const { t } = useTranslation();

  return commit ? (
    <p>
      {t("commitMessage")}
      <span className="inline px-1 font-mono bg-blue-100 rounded">{commit.substring(0, 8)}</span>.
    </p>
  ) : null;
}
