import { PropsWithChildren } from "react";
import { PrefetchPageLinks } from "@remix-run/react";

import PostLink from "~/components/common/PostLink";

export type StepLinkProps = PropsWithChildren<{ targetStep: number }>;

export default function StepLink({ targetStep, children }: StepLinkProps) {
  return (
    <PostLink
      params={{
        targetStep: targetStep.toString(),
        actionName: "changeStep",
      }}
      to="/tutorial"
    >
      {children}

      <PrefetchPageLinks page={`/tutorial/${targetStep}`} />
    </PostLink>
  );
}
