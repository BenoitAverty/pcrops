import invariant from "tiny-invariant";
import { User } from "@prisma/client";
import { useTranslation } from "react-i18next";
import { redirect } from "@remix-run/server-runtime";
import { Form } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import { changeTutorialStep } from "~/lib/users/datastore";
import { ComponentActions } from "~/lib/common/component-actions";

export type StepsProps = {
  step: number;
};

const steps = ["stepsWelcome", "stepsCrops", "stepsSowingPlanting", "stepsZoning", "stepsFinished"];

export const tutorialStepsActions: (user: User) => ComponentActions = (user) => ({
  async changeStep(formData) {
    const targetStep = formData.get("targetStep") as string;
    invariant(targetStep, "target step must be provided");
    invariant(parseInt(targetStep) <= steps.length, "target step does not exist");

    await changeTutorialStep(user, parseInt(targetStep));

    return redirect(`/tutorial/${targetStep}`);
  },
});

export default function Steps({ step }: StepsProps) {
  const { t } = useTranslation();

  const stepIndex = step - 1;

  return (
    <div className="w-full py-6">
      <div className="flex">
        {steps.map((label, index) => (
          <div key={label} className="w-1/4">
            <Form method="post">
              <input name="targetStep" type="hidden" value={index + 1} />
              <div className="relative mb-2">
                {index > 0 && (
                  <div
                    className="absolute flex align-center items-center align-middle content-center"
                    style={{
                      width: "calc(100% - 2.5rem - 1rem)",
                      top: "50%",
                      transform: "translate(-50%, -50%)",
                    }}
                  >
                    <div className="w-full bg-gray-200 rounded items-center align-middle align-center flex-1">
                      <div
                        className="w-0 bg-green-300 py-1 rounded"
                        style={{ width: stepIndex >= index ? "100%" : 0 }}
                      />
                    </div>
                  </div>
                )}
                <button
                  className={`w-10 h-10 mx-auto ${
                    stepIndex >= index
                      ? "bg-green-500 text-white"
                      : "bg-white text-gray-600 border-2 border-gray-200"
                  } rounded-full text-lg text-white flex items-center`}
                  name="actionName"
                  value="changeStep"
                >
                  <span className="text-center w-full">{index + 1}</span>
                </button>
              </div>

              <button
                className={`block m-auto text-xs text-center md:text-base ${
                  index === stepIndex ? "font-bold" : ""
                }`}
                name="actionName"
                value="changeStep"
              >
                {t(label)}
              </button>
            </Form>
          </div>
        ))}
      </div>
    </div>
  );
}
