import React, { useRef } from "react";
import { SerializeFrom } from "@remix-run/server-runtime";
import { useSubmit } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import { PlannedCrop } from "~/lib/crops/model";
import CalendarRow from "~/components/calendar/CalendarRow";
import CropEditForm from "~/components/crops/planning/CropEditForm";
import CropPlanInput from "~/components/crops/planning/CropPlanInput";

export type TutorialCropRowProps = { crop: SerializeFrom<PlannedCrop> };

/**
 * Like the DraggableCropRow except it can't be dragged (and so it can be used outside of a DragDropContext)
 */
export default function TutorialCropRow({ crop }: TutorialCropRowProps) {
  const submit = useSubmit();
  const editCropButtonRef = useRef(null);

  return (
    <CalendarRow key={crop.tid}>
      {{
        heading: (
          <CropEditForm crop={crop} id={`crop-${crop.tid}`} submitButtonRef={editCropButtonRef} />
        ),
        body: (
          <CropPlanInput
            defaultValue={crop.plan}
            form={`crop-${crop.tid}`}
            name="plan"
            onChange={() => submit(editCropButtonRef.current)}
          />
        ),
      }}
    </CalendarRow>
  );
}
