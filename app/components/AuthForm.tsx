import { Form, FormProps } from "@remix-run/react";

type AuthFormProps = {
  buttonLabel: string;
} & Omit<FormProps, "children" | "className">;

export default function AuthForm({ buttonLabel, ...formProps }: AuthFormProps) {
  return (
    <Form className="flex flex-col items-center m-auto" {...formProps}>
      <input
        className="w-3/4 p-2 m-1 rounded border border-black grow focus:border-blue-400"
        name="email"
        placeholder="Your email address"
        type="email"
      />
      <button className="button-primary" type="submit">
        {buttonLabel}
      </button>
    </Form>
  );
}
