import { GardenZone } from "@prisma/client";
import { DragDropContext, Draggable, Droppable, DropResult } from "react-beautiful-dnd";
import invariant from "tiny-invariant";
import { useEffect, useState } from "react";
import { clsx } from "clsx";
import { useTranslation } from "react-i18next";
import { SerializeFrom } from "@remix-run/server-runtime";
import { useSubmit } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import { reorder } from "~/lib/zones/model";
import { ComponentActions } from "~/lib/common/component-actions";
import { updateMany } from "~/lib/zones/datastore";
import ZoneDeleteButton from "~/components/zones/ZoneDeleteButton";

export const actions: ComponentActions = {
  async reorder(formData) {
    const newZones = formData.get("newZones");
    invariant(typeof newZones === "string");

    await updateMany(JSON.parse(newZones));
  },
};

export type GardenZonesListProps = {
  zones: SerializeFrom<GardenZone>[];
};

export default function GardenZonesList({ zones }: GardenZonesListProps) {
  const { t } = useTranslation();
  const submit = useSubmit();
  const [currentZones, setCurrentZones] = useState(zones);
  useEffect(() => {
    setCurrentZones(zones);
  }, [zones]);

  function handleDragEnd(result: DropResult) {
    if (!result.destination) return;

    const newZones = reorder(zones, result.source.index, result.destination.index);
    setCurrentZones(newZones);

    submit(
      {
        actionName: "reorder",
        newZones: JSON.stringify(newZones),
      },
      { method: "post" },
    );
  }

  return (
    <div className="list-inside rounded-lg mt-5 p-1 bg-green-100">
      {zones && zones.length ? (
        <DragDropContext onDragEnd={handleDragEnd}>
          <Droppable droppableId="zonesList">
            {(provided) => (
              <ul {...provided.droppableProps} ref={provided.innerRef}>
                {currentZones.map((z, index) => (
                  <GardenZoneItem key={`zoneItem-${z.tid}`} index={index} zone={z} />
                ))}
                {provided.placeholder}
              </ul>
            )}
          </Droppable>
        </DragDropContext>
      ) : (
        <p className="p-2">{t("emptyZonesMessage")}</p>
      )}
    </div>
  );
}

function GardenZoneItem({ zone, index }: { zone: SerializeFrom<GardenZone>; index: number }) {
  return (
    <Draggable draggableId={zone.tid} index={index}>
      {(provided, { isDragging, isDropAnimating }) => (
        <li
          ref={provided.innerRef}
          className={clsx("flex bg-white m-1", isDragging && !isDropAnimating && "shadow-lg")}
          {...provided.draggableProps}
        >
          <div className="drag-handle" {...provided.dragHandleProps}>
            &nbsp;
          </div>
          <div className="p-2 grow">
            <h6 className="font-bold">{zone.name}</h6>{" "}
            {zone.description && <p className="text-sm">{zone.description}</p>}
          </div>
          <div className="p-2">
            <ZoneDeleteButton tid={zone.tid} />
          </div>
        </li>
      )}
    </Draggable>
  );
}
