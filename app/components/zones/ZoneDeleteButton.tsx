import invariant from "tiny-invariant";
import { Form } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import DeleteIcon from "~/components/common/DeleteIcon";
import { ComponentAction } from "~/lib/common/component-actions";
import { drop } from "~/lib/zones/datastore";

export type ZoneDeleteButtonProps = { tid: string };

export const deleteGardenZone: ComponentAction = async function deleteGardenZone(formData) {
  const zoneTid = formData.get("zoneTid");
  invariant(typeof zoneTid === "string");
  await drop(zoneTid);
};

// TODO optimistic UI
export default function ZoneDeleteButton({ tid }: ZoneDeleteButtonProps) {
  return (
    <Form className="[height:24px]" method="post">
      <input name="zoneTid" type="hidden" value={tid} />
      <button name="actionName" type="submit" value="deleteGardenZone">
        <DeleteIcon />
      </button>
    </Form>
  );
}
