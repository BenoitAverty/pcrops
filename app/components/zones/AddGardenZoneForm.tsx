import invariant from "tiny-invariant";
import { Prisma, User } from "@prisma/client";
import { useTranslation } from "react-i18next";
import { TFunction } from "i18next";
import { clsx } from "clsx";
import { json } from "@remix-run/server-runtime";
import { Form, useActionData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import { ComponentActions } from "~/lib/common/component-actions";
import { create } from "~/lib/zones/datastore";
import { useFormReset } from "~/components/common/utils";
import FormErrors from "~/components/common/FormErrors";

export const addGardenZone: (user: User, t: TFunction) => ComponentActions = (user, t) => ({
  async addGardenZone(formData) {
    const zoneName = formData.get("zoneName");
    const zoneDesc = formData.get("zoneDesc") as string | null;
    invariant(typeof zoneName === "string", "Zone name must be provided");

    try {
      await create(user.tid, zoneName, zoneDesc);
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        if (e.code === "P2002") {
          return json({
            errors: {
              zoneName: t("zoneNameExists"),
            },
          });
        }
      }
    }
  },
});

export default function AddGardenZoneForm() {
  const { t } = useTranslation();

  const actionData = useActionData();
  console.log(actionData);

  const { formRef, inputRef } = useFormReset(["addGardenZone"]);

  return (
    <Form ref={formRef} className="flex flex-col grow items-center m-auto w-1/2" method="post">
      <FormErrors errors={actionData?.errors} />
      <input
        ref={inputRef}
        aria-label={t("zoneName")}
        className={clsx("form-add-text-input self-stretch", {
          "bg-red-100 placeholder:text-red-500": actionData?.errors["zoneName"],
        })}
        name="zoneName"
        placeholder={t("zoneName")}
        required
        type="text"
      />
      <input
        aria-label={t("zoneDesc")}
        className="form-add-text-input self-stretch"
        name="zoneDesc"
        placeholder={t("zoneDesc")}
        type="text"
      />
      <button
        className="button-primary shrink"
        name="actionName"
        type="submit"
        value="addGardenZone"
      >
        {t("addGardenZone")}
      </button>
    </Form>
  );
}
