export type FormErrorsProps = { errors?: { [inputName: string]: string } };

export default function FormErrors({ errors }: FormErrorsProps) {
  if (!errors) return null;

  return (
    <ul className="w-full align-left list-disc list-inside text-red-500">
      {Object.values(errors).map((e) => (
        <li key={e}>{e}</li>
      ))}
    </ul>
  );
}
