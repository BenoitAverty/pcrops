import { useEffect, useRef } from "react";
import { useTransition } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

/**
 * Reset a form when it finishes a submission with one of the given actionNames.
 *
 * The action name must be in the `actionName` key of the submitted formData.
 *
 * @return an object with two refs: one to the form that must be reset and one
 * to the input that will receive focus.
 */
export function useFormReset(actionNames: string[]) {
  const transition = useTransition();
  const submittingAction =
    transition.state === "submitting" && transition.submission.formData.get("actionName");
  const isSubmitting =
    typeof submittingAction === "string" && actionNames.includes(submittingAction);

  useEffect(() => {
    if (!isSubmitting) {
      formRef.current?.reset();
      inputRef.current?.focus();
    }
  }, [isSubmitting]);

  const formRef = useRef<HTMLFormElement | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);

  return { formRef, inputRef };
}
