export default function DeleteIcon() {
  return <img alt="delete" height={24} src="/delete.svg" width={24} />;
}
