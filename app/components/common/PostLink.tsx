import { Form, LinkProps, PrefetchPageLinks } from "@remix-run/react";

export type PostLinkProps = LinkProps & { params?: { [key: string]: string } };

/**
 * Link that makes a POST request instead of a GET request to the destination.
 *
 * Renders a <form method="post"> instead of a <a> tag. The parameters are added as hidden inputs.
 */
export default function PostLink({
  params = {},
  to,
  reloadDocument,
  replace,
  children,
}: PostLinkProps) {
  const action = typeof to === "string" ? to : (to.pathname || "") + to.search + to.hash;
  const pagePath = typeof to === "string" ? to.split("?")[0].split("#")[0] : to.pathname || "";

  return (
    <Form action={action} method="post" reloadDocument={reloadDocument} replace={replace}>
      {Object.entries(params).map(([k, v]) => (
        <input key={k} name={k} type="hidden" value={v} />
      ))}
      <button className="underline text-blue-900 hover:font-extrabold" type="submit">
        {children}
      </button>
      <PrefetchPageLinks page={pagePath} />
    </Form>
  );
}
