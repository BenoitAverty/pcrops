import { useState } from "react";
import { Trans, useTranslation } from "react-i18next";

export type PlanningYearProps = { children: number };

// TODO : extract tooltip component
export default function PlanningYear({ children: year }: PlanningYearProps) {
  const { t } = useTranslation();
  const [tooltip, setTooltip] = useState(false);

  return (
    <span className="inline-flex relative flex-col items-center">
      <span
        className="px-1 border-b-2 border-black border-dotted cursor-pointer"
        data-tooltip-target="tooltip-default"
        onClick={() => setTooltip((b) => !b)}
      >
        {year}
      </span>
      <span
        className={`absolute bottom-0 ${
          tooltip ? "flex" : "hidden"
        } min-w-max flex-col items-center mb-8`}
        onClick={() => setTooltip(false)}
      >
        <span className="relative z-10 p-2 text-sm font-normal text-center text-black bg-indigo-300 rounded whitespace-no-wrap">
          <Trans i18nKey="calendarYear" t={t}>
            first line <br /> second line
          </Trans>
        </span>
        <span className="-mt-2 w-3 h-3 bg-indigo-300 rotate-45" />
      </span>
    </span>
  );
}
