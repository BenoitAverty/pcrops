// These are to compute width of all table cells. It is required to make it work nicely with drag&drop
const tableWidthPx = 1500;
const headingColWidthPx = 160;

export const tableWidthStyle = () => ({ width: `${tableWidthPx}px` });

export const headingCellWidthStyle = () => ({ width: `${headingColWidthPx}px` });

export const weekCellWidthStyle = () => ({
  width: `${((tableWidthPx - headingColWidthPx) / 156) * 3}px`,
});

export const monthCellWidthStyle = (colspan: number) => ({
  width: `${((tableWidthPx - headingColWidthPx) / 156) * colspan}px`,
});
