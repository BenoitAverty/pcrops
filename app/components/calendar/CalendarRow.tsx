import { ForwardedRef, forwardRef, HTMLProps, ReactNode } from "react";

import { headingCellWidthStyle } from "~/components/calendar/utils";

export type CalendarRowProps = {
  children: {
    heading: ReactNode;
    body: ReactNode;
  };
} & Omit<HTMLProps<HTMLTableRowElement>, "children">;

function CalendarRow(
  { children: { heading, body }, ...trProps }: CalendarRowProps,
  ref: ForwardedRef<HTMLTableRowElement>,
) {
  return (
    <tr {...trProps} ref={ref} className="bg-gray-100">
      <th
        className="p-0 text-left border-b border-gray-100 indent-2 calendar"
        style={headingCellWidthStyle()}
      >
        {heading}
      </th>
      {body}
    </tr>
  );
}

export default forwardRef<HTMLTableRowElement, CalendarRowProps>(CalendarRow);
