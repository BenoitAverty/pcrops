import { ForwardedRef, forwardRef, HTMLProps, ReactNode } from "react";

import { useCalendarPeriod } from "~/components/calendar/Calendar";
import { makeWeeks } from "~/lib/calendar/calendar-utils";

export type RowGroupProps = {
  title?: string;
  children: ReactNode;
} & HTMLProps<HTMLTableSectionElement>;

function RowGroup(
  { title, children, ...droppableProvided }: RowGroupProps,
  ref: ForwardedRef<HTMLTableSectionElement>,
) {
  const period = useCalendarPeriod();
  const nWeeks = makeWeeks(
    period.startingYear,
    period.startingMonth,
    period.durationInMonths,
  ).length;

  return (
    <tbody ref={ref} {...droppableProvided} className="border-y-8 border-white">
      {title && (
        <tr className="bg-slate-300">
          <th className="pl-2 text-left" colSpan={3 * nWeeks + 1}>
            <h6>{title}</h6>
          </th>
        </tr>
      )}
      {children}
    </tbody>
  );
}

export default forwardRef(RowGroup);
