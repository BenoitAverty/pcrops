import { ReactNode } from "react";

import { weekCellWidthStyle } from "~/components/calendar/utils";

export type WeekCellProps = { children: ReactNode };

export default function WeekCell({ children }: WeekCellProps) {
  return (
    <td colSpan={3} style={weekCellWidthStyle()}>
      {children}
    </td>
  );
}
