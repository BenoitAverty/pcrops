import { useTranslation } from "react-i18next";
import { Context, createContext, PropsWithChildren, ReactNode, useContext } from "react";
import invariant from "tiny-invariant";

import {
  headingCellWidthStyle,
  monthCellWidthStyle,
  tableWidthStyle,
} from "~/components/calendar/utils";
import {
  monthNameIntl,
  makeMonths,
  makeWeeks,
  CalendarPeriod,
} from "~/lib/calendar/calendar-utils";

type CalendarProps = PropsWithChildren<{
  showBottomHeading?: boolean;
  topLeftHeading?: ReactNode;
  period: CalendarPeriod;
}>;

export default function Calendar({
  children,
  showBottomHeading = false,
  topLeftHeading = null,
  period,
}: CalendarProps) {
  const { t } = useTranslation();

  return (
    <PeriodContext.Provider value={period}>
      <div className="p-0 my-3 mx-auto overflow-x-auto overflow-y-visible pb-4">
        <table className="select-none table-fixed mx-auto" style={tableWidthStyle()}>
          <thead>
            <tr>
              <th style={headingCellWidthStyle()}>&nbsp;</th>
              {makeMonths(
                period.startingYear,
                period.startingMonth,
                period.durationInMonths,
                monthNameIntl(t),
              ).map((m) => (
                <th
                  key={m.name}
                  className="even:bg-slate-500 odd:bg-slate-600 text-gray-100 second:rounded-tl-md last:rounded-tr-md"
                  colSpan={m.colspan}
                  style={monthCellWidthStyle(m.colspan)}
                >
                  {m.name}
                </th>
              ))}
            </tr>
            <tr>
              <th className="bg-slate-400 text-gray-100 rounded-tl-md">
                {topLeftHeading || <>&nbsp;</>}
              </th>
              {makeWeeks(period.startingYear, period.startingMonth, period.durationInMonths).map(
                (w) => (
                  <th
                    key={`${w.year}-${w.number}`}
                    className="bg-slate-400 text-gray-100"
                    colSpan={3}
                  >
                    <div className="m-0 p-0">
                      <div className="m-0 p-0 invisible h-0">00</div>
                      {w.number}
                    </div>
                  </th>
                ),
              )}
            </tr>
          </thead>
          {children}
          {showBottomHeading && (
            <thead>
              <tr>
                <th>&nbsp;</th>
                {makeWeeks(period.startingYear, period.startingMonth, period.durationInMonths).map(
                  (w) => (
                    <th
                      key={`${w.year}-${w.number}`}
                      className="bg-slate-400 text-gray-100"
                      colSpan={3}
                    >
                      <div className="m-0 p-0">
                        <div className="m-0 p-0 invisible h-0">00</div>
                        {w.number}
                      </div>
                    </th>
                  ),
                )}
              </tr>
              <tr>
                <th style={headingCellWidthStyle()}>&nbsp;</th>
                {makeMonths(
                  period.startingYear,
                  period.startingMonth,
                  period.durationInMonths,
                  monthNameIntl(t),
                ).map((m) => (
                  <th
                    key={m.name}
                    className="even:bg-slate-500 odd:bg-slate-600 text-gray-100 second:rounded-bl-md last:rounded-br-md"
                    colSpan={m.colspan}
                    style={monthCellWidthStyle(m.colspan)}
                  >
                    {m.name}
                  </th>
                ))}
              </tr>
            </thead>
          )}
        </table>
      </div>
    </PeriodContext.Provider>
  );
}

const PeriodContext: Context<CalendarPeriod | null> = createContext<CalendarPeriod | null>(null);
export function useCalendarPeriod(): CalendarPeriod {
  const period = useContext(PeriodContext);

  invariant(period, "Calendar period is not defined");

  return period;
}
