import { ReactNode } from "react";

export default function EmptyRow({ children }: { children: ReactNode }) {
  return (
    <tr>
      <th className="bg-slate-100" colSpan={157}>
        {children}
      </th>
    </tr>
  );
}
