import { useChangeLanguage } from "remix-i18next";
import { json, LinksFunction, LoaderFunction } from "@remix-run/server-runtime";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useCatch,
  useLoaderData,
} from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069

import Layout from "~/components/layout/Layout";
// eslint-disable-next-line import/no-unresolved
import tailwindCss from "~/tailwind.css";
import remixI18Next from "~/lib/common/i18next.server";

export const links: LinksFunction = () => [{ rel: "stylesheet", href: tailwindCss }];

export const loader: LoaderFunction = async function rootLoader({ request }) {
  return json({
    locale: await remixI18Next.getLocale(request),
    commit: process.env.COMMIT_SHA1,
  });
};

export default function App() {
  const { locale, commit } = useLoaderData();
  useChangeLanguage(locale);

  return (
    <Document lang={locale} title="PCrops">
      <Layout commit={commit}>
        <Outlet />
      </Layout>
    </Document>
  );
}

function Document({
  children,
  title,
  lang,
}: {
  children: React.ReactNode;
  title?: string;
  lang?: string;
}) {
  return (
    <html lang={lang}>
      <head>
        <meta charSet="utf-8" />
        <meta content="width=device-width,initial-scale=1" name="viewport" />
        {title ? <title>{title}</title> : null}
        <Meta />
        <Links />
      </head>
      <body>
        {children}
        <ScrollRestoration />
        <Scripts />
        {process.env.NODE_ENV === "development" && <LiveReload />}
      </body>
    </html>
  );
}

export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);

  return (
    <Document>
      <Layout>
        <div>
          <h1>There was an error</h1>
          <p>{error.message}</p>
        </div>
      </Layout>
    </Document>
  );
}

export function CatchBoundary() {
  const caught = useCatch();

  console.log(caught);

  let message;
  switch (caught.status) {
    case 401:
      message = <p>Oops! Looks like you tried to visit a page that you do not have access to.</p>;
      break;
    case 404:
      message = <p>Oops! Looks like you tried to visit a page that does not exist.</p>;
      break;

    default:
      throw new Error(caught.data || caught.statusText);
  }

  return (
    <Document>
      <Layout>
        <h1>
          {caught.status}: {caught.statusText}
        </h1>
        {message}
      </Layout>
    </Document>
  );
}
