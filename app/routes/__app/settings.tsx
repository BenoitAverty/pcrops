import { useTranslation } from "react-i18next";

import StepLink from "~/components/tutorial/StepLink";
import PostLink from "~/components/common/PostLink";

export default function Settings() {
  const { t } = useTranslation();
  return (
    <section>
      <h1>{t("settingsTitle")}</h1>

      <p>
        <StepLink targetStep={1}>{t("redoTutorial")}</StepLink>
      </p>

      <p>
        <PostLink to="/logout">{t("logoutLink")}</PostLink>
      </p>
    </section>
  );
}
