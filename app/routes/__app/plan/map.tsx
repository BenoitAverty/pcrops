import { GardenZone } from "@prisma/client";
import { useTranslation } from "react-i18next";
import { ActionFunction, json, LoaderFunction, redirect } from "@remix-run/server-runtime";
import { useLoaderData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import { requireUserOrCreate, requireUserOrFail } from "~/lib/users/route-utils";
import { findByOwnerId } from "~/lib/zones/datastore";
import { deleteGardenZone } from "~/components/zones/ZoneDeleteButton";
import { applyActions } from "~/lib/common/component-actions";
import GardenZonesList, { actions as zonesListActions } from "~/components/zones/GardenZonesList";
import AddGardenZoneForm, { addGardenZone } from "~/components/zones/AddGardenZoneForm";
import remixI18Next from "~/lib/common/i18next.server";

type LoaderData = {
  zones: GardenZone[];
};

export const loader: LoaderFunction = async function planMapLoader({ request }) {
  const user = await requireUserOrCreate(request, request.url);

  const zones = await findByOwnerId(user.tid);

  return json<LoaderData>({
    zones,
  });
};

export const action: ActionFunction = async function planMapAction({ request }) {
  const user = await requireUserOrFail(request);

  const data = await request.formData();

  const actionResponse = await applyActions(data, {
    ...addGardenZone(user, await remixI18Next.getFixedT(request, "plan")),
    deleteGardenZone,
    ...zonesListActions,
  });

  return actionResponse || redirect("/plan/map");
};

export default function PlanMap() {
  const { t } = useTranslation();
  const { zones } = useLoaderData<LoaderData>();

  return (
    <>
      <section className="pt-5 w-3/4 m-auto">
        <h2 className="text-center">{t("planMapHeading")}</h2>

        <AddGardenZoneForm />

        <GardenZonesList zones={zones} />
      </section>
    </>
  );
}
