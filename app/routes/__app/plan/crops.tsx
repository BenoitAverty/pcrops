import { useTranslation } from "react-i18next";
import { ActionFunction, json, LoaderFunction, redirect } from "@remix-run/server-runtime";
import { useLoaderData } from "@remix-run/react";

import { requireUserOrCreate, requireUserOrFail } from "~/lib/users/route-utils";
import { findByOwnerId as findCrops } from "~/lib/crops/datastore";
import { FullCrop } from "~/lib/crops/model";
import PlanningYear from "~/components/PlanningYear";
import { applyActions } from "~/lib/common/component-actions";
import CalendarKey from "~/components/crops/planning/CalendarKey";
import AddCropForm, { addCropActions } from "~/components/crops/AddCropForm";
import { groupByZone, GroupedCrops } from "~/lib/zones/model";
import { findByOwnerId as findZones } from "~/lib/zones/datastore";
import EditablePlanning, {
  actions as editablePlanningActions,
} from "~/components/crops/planning/EditablePlanning";
import { CalendarPeriod, DEFAULT_YEAR } from "~/lib/calendar/calendar-utils";

type LoaderData = {
  calendarPeriod: CalendarPeriod;
  groupedCrops: GroupedCrops<FullCrop>;
};

export const loader: LoaderFunction = async function planCropsLoader({ request }) {
  const user = await requireUserOrCreate(request, request.url);

  // TODO: make this configurable by the user
  const calendarPeriod = {
    startingYear: DEFAULT_YEAR,
    startingMonth: 0,
    durationInMonths: 12,
  };

  const [crops, zones] = await Promise.all([
    findCrops(user.tid, calendarPeriod),
    findZones(user.tid),
  ]);

  return json<LoaderData>({
    calendarPeriod,
    groupedCrops: groupByZone(crops, zones),
  });
};

export const action: ActionFunction = async function planCropsAction({ request }) {
  const user = await requireUserOrFail(request);

  const data = await request.formData();

  await applyActions(data, {
    ...addCropActions(user),
    ...editablePlanningActions,
  });

  return redirect("/plan/crops");
};

export default function PlanCrops() {
  const { groupedCrops, calendarPeriod } = useLoaderData<LoaderData>();
  const { t } = useTranslation();

  return (
    <section className="pt-5">
      <h2 className="text-center">
        {t("planningYearHeading")} <PlanningYear>{DEFAULT_YEAR}</PlanningYear>
      </h2>

      <AddCropForm />

      <EditablePlanning calendarPeriod={calendarPeriod} groupedCrops={groupedCrops} />
      <CalendarKey />
    </section>
  );
}
