import { useTranslation } from "react-i18next";
import { NavLink, Outlet } from "@remix-run/react";

export default function Plan() {
  const { t } = useTranslation();

  return (
    <>
      <nav className="mt-1">
        <ul className="flex flex-row gap-x-5 justify-center">
          <li>
            <NavLink prefetch="intent" to="/plan/crops">
              {t("planCropsLink")}
            </NavLink>
          </li>
          <li>
            <NavLink prefetch="intent" to="/plan/map">
              {t("planMapLink")}
            </NavLink>
          </li>
        </ul>
      </nav>
      <Outlet />
    </>
  );
}
