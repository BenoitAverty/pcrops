import { Link, Outlet, useLoaderData } from "@remix-run/react";
import { json, LoaderFunction } from "@remix-run/server-runtime";
import { Trans, useTranslation } from "react-i18next";

import { getUserOrNull } from "~/lib/users/route-utils";
import { isRegistered } from "~/lib/users/model";

export const loader: LoaderFunction = async function appLayoutLoader({ request }) {
  const user = await getUserOrNull(request);

  return json({ isRegistered: user && isRegistered(user) });
};

export default function AppLayout() {
  const { t } = useTranslation();
  const { isRegistered }: { isRegistered: boolean } = useLoaderData();

  return (
    <>
      <Outlet />
      {!isRegistered && (
        <aside>
          {/* TODO : style and translate this */}
          <p className="text-center mt-10 alert" data-testid="unregistered-alert">
            <Trans i18nKey="unregisteredAlert" t={t}>
              You are not registered. Your data is saved for some time but you won&apos;t be able to
              access it from another device. <Link to="/register">Register</Link> or{" "}
              <Link to="login">Log in</Link> to save your work safely.
            </Trans>
          </p>
        </aside>
      )}
    </>
  );
}
