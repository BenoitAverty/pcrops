import { Link } from "@remix-run/react";

export default function LandingPage() {
  return (
    <section className="mt-6 text-center w-3/4 mx-auto">
      <h2>Welcome</h2>
      <div className="flex mx-auto justify-center">
        {/* TODO : actually do the page */}
        <Link className="mx-3" prefetch="render" to="/plan/crops">
          Go to app
        </Link>
        <Link to="/tutorial">Do the tutorial</Link>
      </div>
    </section>
  );
}
