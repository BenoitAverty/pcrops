import { json, LoaderFunction } from "@remix-run/server-runtime";
import { useLoaderData } from "@remix-run/react";

export const loader: LoaderFunction = async function authConfirmLoader({ request }) {
  const searchParams = new URL(request.url).searchParams;
  const type = searchParams.get("type");
  const email = searchParams.get("email");

  return json({ type, email });
};

export default function AuthConfirm() {
  const { type, email } = useLoaderData();

  let content;
  if (type === "register") {
    content = (
      <>
        <h2>Thanks</h2>
        <p>
          An email has been sent to {email || "your inbox"}. Click the link inside to finish the
          registration process.
        </p>
      </>
    );
  } else if (type === "login") {
    content = (
      <p>An email has been sent to {email || "your inbox"}. Click the link inside to log-in.</p>
    );
  } else {
    content = <p>Check your inbox to finish the process.</p>;
  }

  return <section className="mt-6 text-center w-3/4 mx-auto">{content}</section>;
}
