import { ActionFunction, json, LoaderFunction } from "@remix-run/server-runtime";
import { Outlet, useLoaderData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import Steps, { tutorialStepsActions } from "../components/tutorial/Steps";

import { requireUserOrFail, requireUserAndRedirectToTutorialStep } from "~/lib/users/route-utils";
import { applyActions } from "~/lib/common/component-actions";

type LoaderData = {
  tutorialStep: number;
};

export const loader: LoaderFunction = async function tutorialLoader({ request }) {
  const user = await requireUserAndRedirectToTutorialStep(request);

  return json<LoaderData>({
    tutorialStep: user.tutorialStep,
  });
};

export const action: ActionFunction = async function tutorialAction({ request }) {
  const user = await requireUserOrFail(request);

  const data = await request.formData();

  return await applyActions(data, tutorialStepsActions(user));
};

export default function Tutorial() {
  const { tutorialStep } = useLoaderData<LoaderData>();

  return (
    <div>
      <Steps step={tutorialStep} />
      <Outlet />
    </div>
  );
}
