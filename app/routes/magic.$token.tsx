import invariant from "tiny-invariant";
import { LoaderFunction, redirect } from "@remix-run/server-runtime";
import { useCatch, useLoaderData } from "@remix-run/react";

import { verifyToken } from "~/lib/auth/tokens";
import { loginUser, registerUser } from "~/lib/users/model";
import { createUserSession } from "~/lib/users/session.server";

export const loader: LoaderFunction = async function verifyMagicLink({ params }) {
  invariant(params.token);

  const claims = verifyToken(params.token);

  let user;
  if (claims.type === "register") {
    user = await registerUser(claims.sub, claims.email);
  } else if (claims.type === "login") {
    user = await loginUser(claims.email);
  } else {
    throw new Error("wtf");
  }

  return redirect("/plan/crops", {
    headers: await createUserSession(user.tid),
  });
};

export default function MagicConfirm() {
  const data = useLoaderData();

  return (
    <pre>
      <code>{JSON.stringify(data)}</code>
    </pre>
  );
}

export function CatchBoundary() {
  const { data } = useCatch();

  return (
    <pre>
      <code>{JSON.stringify(data)}</code>
    </pre>
  );
}
