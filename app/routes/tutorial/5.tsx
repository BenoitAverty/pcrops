import { Trans, useTranslation } from "react-i18next";
import { Link } from "@remix-run/react";

export default function TutorialFinish() {
  const { t } = useTranslation();

  return (
    <section>
      <h2>{t("stepFinishTitle")}</h2>

      <p>{t("stepFinishParagraph1")}</p>

      <p>
        <Trans i18nKey="stepFinishParagraph2" t={t}>
          begin <Link to="/register">register</Link> end.
        </Trans>
      </p>

      <Link to="/plan/crops">{t("goToApp")}</Link>
    </section>
  );
}
