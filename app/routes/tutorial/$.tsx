import { LoaderFunction } from "@remix-run/server-runtime";

import { requireUserAndRedirectToTutorialStep } from "../../lib/users/route-utils";

import { DEFAULT_YEAR } from "~/lib/calendar/calendar-utils";

export const loader: LoaderFunction = async function unknownTutorialLoader({ request }) {
  await requireUserAndRedirectToTutorialStep(request);
};

export const tutorialCalendarPeriod = {
  startingYear: DEFAULT_YEAR,
  startingMonth: 0,
  durationInMonths: 12,
};
