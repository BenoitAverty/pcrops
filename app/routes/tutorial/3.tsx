import { useTranslation } from "react-i18next";
import { ActionFunction, json, LoaderFunction, redirect } from "@remix-run/server-runtime";
import React from "react";
import { useLoaderData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import EmptyRow from "~/components/calendar/EmptyRow";
import StepLink from "~/components/tutorial/StepLink";
import AddCropFrom, { addCropActions } from "~/components/crops/AddCropForm";
import Calendar from "~/components/calendar/Calendar";
import CalendarKey from "~/components/crops/planning/CalendarKey";
import { requireUserAndRedirectToTutorialStep, requireUserOrFail } from "~/lib/users/route-utils";
import { findByOwnerId } from "~/lib/crops/datastore";
import { PlannedCrop } from "~/lib/crops/model";
import { applyActions } from "~/lib/common/component-actions";
import { actions as cropEditFormActions } from "~/components/crops/planning/CropEditForm";
import RowGroup from "~/components/calendar/RowGroup";
import TutorialCropRow from "~/components/tutorial/TutorialCropRow";
import { tutorialCalendarPeriod } from "~/routes/tutorial/$";
import { CalendarPeriod } from "~/lib/calendar/calendar-utils";

type LoaderData = {
  crops: PlannedCrop[];
  calendarPeriod: CalendarPeriod;
};

export const loader: LoaderFunction = async function tutorial3loader({ request }) {
  const user = await requireUserAndRedirectToTutorialStep(request);
  const crops = await findByOwnerId(user.tid, tutorialCalendarPeriod);

  return json<LoaderData>({
    crops,
    calendarPeriod: tutorialCalendarPeriod,
  });
};

export const action: ActionFunction = async function tutorial3Action({ request }) {
  const user = await requireUserOrFail(request);
  const data = await request.formData();

  await applyActions(data, {
    ...addCropActions(user),
    ...cropEditFormActions,
  });

  return redirect("/tutorial/2");
};

export default function Tutorial3() {
  const { t } = useTranslation();
  const { crops, calendarPeriod } = useLoaderData<LoaderData>();

  return (
    <>
      <section>
        <h2>{t("step3Title")}</h2>

        <p>{t("step3Paragraph1")}</p>

        <div className="tutorial">
          <p>{t("step3Tutorial")}</p>
          <AddCropFrom />
        </div>

        <Calendar period={calendarPeriod} topLeftHeading={t("speciesNameColumnHeading")}>
          <RowGroup>
            {crops.length > 0 ? (
              crops.map((c) => <TutorialCropRow key={c.tid} crop={c} />)
            ) : (
              <EmptyRow>{t("emptyCalendarMessage")}</EmptyRow>
            )}
          </RowGroup>
        </Calendar>

        <CalendarKey items={["sowing", "planting", "growth", "harvest"]} />
      </section>
      <section>
        <h2>{t("step2FinishTitle")}</h2>
        <StepLink targetStep={4}>{t("stepFinishLink")}</StepLink>
      </section>
    </>
  );
}
