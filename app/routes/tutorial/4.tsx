import { useTranslation } from "react-i18next";
import { ActionFunction, json, LoaderFunction, redirect } from "@remix-run/server-runtime";
import { GardenZone } from "@prisma/client";
import React from "react";
import { useLoaderData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import StepLink from "../../components/tutorial/StepLink";

import AddGardenZoneForm, { addGardenZone } from "~/components/zones/AddGardenZoneForm";
import GardenZonesList, { actions as zonesListActions } from "~/components/zones/GardenZonesList";
import { requireUserAndRedirectToTutorialStep, requireUserOrFail } from "~/lib/users/route-utils";
import { findByOwnerId as findZones } from "~/lib/zones/datastore";
import { findByOwnerId as findCrops } from "~/lib/crops/datastore";
import { applyActions } from "~/lib/common/component-actions";
import { deleteGardenZone } from "~/components/zones/ZoneDeleteButton";
import EditablePlanning, {
  actions as editablePlanningActions,
} from "~/components/crops/planning/EditablePlanning";
import { groupByZone, GroupedCrops } from "~/lib/zones/model";
import { FullCrop } from "~/lib/crops/model";
import remixI18Next from "~/lib/common/i18next.server";
import { tutorialCalendarPeriod } from "~/routes/tutorial/$";
import { CalendarPeriod } from "~/lib/calendar/calendar-utils";

type LoaderData = {
  calendarPeriod: CalendarPeriod;
  zones: GardenZone[];
  groupedCrops: GroupedCrops<FullCrop>;
};

export const loader: LoaderFunction = async function Tutorial4Loader({ request }) {
  const user = await requireUserAndRedirectToTutorialStep(request);

  const zones = await findZones(user.tid);
  const crops = await findCrops(user.tid, tutorialCalendarPeriod);

  return json<LoaderData>({
    calendarPeriod: tutorialCalendarPeriod,
    zones,
    groupedCrops: groupByZone(crops, zones),
  });
};

export const action: ActionFunction = async function Tutorial4Action({ request }) {
  const user = await requireUserOrFail(request);

  const data = await request.formData();

  const actionResponse = await applyActions(data, {
    ...addGardenZone(user, await remixI18Next.getFixedT(request, "plan")),
    deleteGardenZone,
    ...zonesListActions,
    ...editablePlanningActions,
  });

  return actionResponse || redirect("/plan/map");
};

export default function Tutorial4() {
  const { t } = useTranslation();
  const { zones, groupedCrops, calendarPeriod } = useLoaderData<LoaderData>();

  return (
    <>
      <section>
        <h2>{t("stepZoningTitle")}</h2>

        <p>{t("stepZoningParagraph1")}</p>

        <div className="tutorial">
          <p>{t("stepZoningTutorial1")}</p>

          <AddGardenZoneForm />

          <GardenZonesList zones={zones} />

          <p>{t("stepZoningParagraph2")}</p>
        </div>

        <EditablePlanning calendarPeriod={calendarPeriod} groupedCrops={groupedCrops} />
      </section>
      <section>
        <h2>{t("step2FinishTitle")}</h2>
        <StepLink targetStep={5}>{t("stepFinishLink")}</StepLink>
      </section>
    </>
  );
}
