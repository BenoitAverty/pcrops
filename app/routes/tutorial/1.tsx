import { useTranslation } from "react-i18next";
import { ActionFunction, json, LoaderFunction, redirect } from "@remix-run/server-runtime";
import { PrefetchPageLinks, useLoaderData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import AddCropForm, { addCropActions } from "../../components/crops/AddCropForm";
import PlanningYear from "../../components/PlanningYear";

import { applyActions } from "~/lib/common/component-actions";
import { requireUserOrFail } from "~/lib/users/route-utils";
import { changeTutorialStep } from "~/lib/users/datastore";
import { DEFAULT_YEAR } from "~/lib/calendar/calendar-utils";

type LoaderData = {
  planningYear: number;
};

export const loader: LoaderFunction = async function tutorial1Loader() {
  return json({
    planningYear: DEFAULT_YEAR,
  });
};

export const action: ActionFunction = async function tutorial1Action({ request }) {
  const user = await requireUserOrFail(request);
  const data = await request.formData();

  await applyActions(data, { ...addCropActions(user) });
  await changeTutorialStep(user, 2);

  return redirect("/tutorial/2");
};

export default function Tutorial1() {
  const { t } = useTranslation();
  const { planningYear } = useLoaderData<LoaderData>();

  return (
    <section>
      <h2>{t("step1Title1")}</h2>

      <p>{t("step1Paragraph1")}</p>

      <p>{t("step1Paragraph2")}</p>

      <p>
        {t("step1Paragraph3")}
        <PlanningYear>{planningYear}</PlanningYear>.
      </p>
      <h2>
        {t("step1Title2")}
        <PlanningYear>{planningYear}</PlanningYear> ?
      </h2>

      <div className="tutorial">
        <p>{t("step1Tutorial1")}</p>
        <AddCropForm hideOptions />
        <PrefetchPageLinks page="/tutorial/2" />
      </div>
    </section>
  );
}
