import { ActionFunction, json, LoaderFunction, redirect } from "@remix-run/server-runtime";
import { useTranslation } from "react-i18next";
import React from "react";
import { useLoaderData } from "@remix-run/react"; // Because of https://github.com/remix-run/remix/issues/4080#issuecomment-1229356069;

import CalendarKey from "~/components/crops/planning/CalendarKey";
import AddCropForm, { addCropActions } from "~/components/crops/AddCropForm";
import StepLink from "~/components/tutorial/StepLink";
import { applyActions } from "~/lib/common/component-actions";
import EmptyRow from "~/components/calendar/EmptyRow";
import Calendar from "~/components/calendar/Calendar";
import { findByOwnerId } from "~/lib/crops/datastore";
import { requireUserAndRedirectToTutorialStep, requireUserOrFail } from "~/lib/users/route-utils";
import { PlannedCrop } from "~/lib/crops/model";
import { actions as cropEditFormActions } from "~/components/crops/planning/CropEditForm";
import RowGroup from "~/components/calendar/RowGroup";
import TutorialCropRow from "~/components/tutorial/TutorialCropRow";
import { CalendarPeriod } from "~/lib/calendar/calendar-utils";
import { tutorialCalendarPeriod } from "~/routes/tutorial/$";

type LoaderData = {
  crops: PlannedCrop[];
  calendarPeriod: CalendarPeriod;
};

export const loader: LoaderFunction = async function tutorial2Loader({ request }) {
  const user = await requireUserAndRedirectToTutorialStep(request);
  const crops = await findByOwnerId(user.tid, tutorialCalendarPeriod);

  return json<LoaderData>({
    crops,
    calendarPeriod: tutorialCalendarPeriod,
  });
};

export const action: ActionFunction = async function wizard2Action({ request }) {
  const user = await requireUserOrFail(request);
  const data = await request.formData();

  await applyActions(data, {
    ...addCropActions(user),
    ...cropEditFormActions,
  });

  return redirect("/tutorial/2");
};

export default function Tutorial2() {
  const { t } = useTranslation();
  const { crops, calendarPeriod } = useLoaderData<LoaderData>();

  return (
    <>
      <section>
        <h2>{t("step2Title")}</h2>
        <p>{t("step2Paragraph1")}</p>
        <div className="tutorial">
          <p>{t("step2Paragraph2")}</p>

          <p>{t("step2Paragraph3")}</p>
        </div>

        <AddCropForm hideOptions />

        <Calendar period={calendarPeriod} topLeftHeading={t("speciesNameColumnHeading")}>
          <RowGroup>
            {crops.length > 0 ? (
              crops.map((c) => <TutorialCropRow key={c.tid} crop={c} />)
            ) : (
              <EmptyRow>{t("emptyCalendarMessage")}</EmptyRow>
            )}
          </RowGroup>
        </Calendar>

        <CalendarKey items={["planting", "growth", "harvest"]} />
      </section>
      <section>
        <h2>{t("step2FinishTitle")}</h2>
        <StepLink targetStep={3}>{t("stepFinishLink")}</StepLink>
      </section>
    </>
  );
}
