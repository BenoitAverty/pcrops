import { ActionFunction, redirect } from "@remix-run/server-runtime";
import invariant from "tiny-invariant";
import { useTranslation } from "react-i18next";

import { generateRegisterToken } from "~/lib/auth/tokens";
import { getUserTid } from "~/lib/users/session.server";
import { sendRegisterToken } from "~/lib/auth/mail";
import AuthForm from "~/components/AuthForm";

export const action: ActionFunction = async function registerAction({ request }) {
  const data = await request.formData();
  const email = data.get("email") as string | null;
  invariant(email, "email should not be null");

  const userTid = await getUserTid(request);

  const token = generateRegisterToken(email, userTid);
  await sendRegisterToken(email, new URL(request.url).origin, token);

  return redirect(`/authconfirm?type=register&email=${email}`);
};

export default function RegisterForm() {
  const { t } = useTranslation();

  return (
    <section className="mt-6 text-center w-3/4 mx-auto">
      <h2>{t("Register")}</h2>
      <p>{t("registerMainText")}</p>
      <AuthForm buttonLabel={t("registerButtonLabel")} method="post" />
    </section>
  );
}
