import { LoaderFunction } from "@remix-run/server-runtime";
import { useTranslation } from "react-i18next";

export const loader: LoaderFunction = function catchallLoader() {
  throw new Response(null, { status: 404 });
};

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function Noop(): void {}

export function CatchBoundary() {
  const { t } = useTranslation();

  return (
    <section>
      <h1>{t("notFoundTitle")}</h1>
    </section>
  );
}
