import { ActionFunction, redirect } from "@remix-run/server-runtime";
import invariant from "tiny-invariant";
import { useTranslation } from "react-i18next";

import { generateLoginToken } from "~/lib/auth/tokens";
import { sendLoginToken } from "~/lib/auth/mail";
import AuthForm from "~/components/AuthForm";

export const action: ActionFunction = async function registerAction({ request }) {
  const data = await request.formData();
  const email = data.get("email") as string | null;
  invariant(email, "email should not be null");

  const token = generateLoginToken(email);
  await sendLoginToken(email, new URL(request.url).origin, token);

  return redirect(`/authconfirm?type=login&email=${email}`);
};

export default function LoginForm() {
  const { t } = useTranslation();

  return (
    <section className="mt-6 text-center w-3/4 mx-auto">
      <h2>{t("loginHeading", { defaultValue: "Log in" })}</h2>
      <p>{t("loginMainText")}</p>
      <p className="alert">{t("loginAlert")}</p>
      <AuthForm buttonLabel={t("loginButtonLabel", { defaultValue: "Log in" })} method="post" />
    </section>
  );
}
