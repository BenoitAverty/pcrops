import { ActionFunction, redirect } from "@remix-run/server-runtime";

import { destroyUserSession } from "~/lib/users/session.server";

export const action: ActionFunction = async function logoutAction() {
  return redirect("/", {
    headers: await destroyUserSession(),
  });
};
