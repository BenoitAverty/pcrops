export type Tid = {
  tid: string;
};

export type EntityFields = Tid & {
  createdAt: Date;
  updatedAt: Date;
};

export type Detached<T extends EntityFields> = Omit<T, keyof EntityFields>;
