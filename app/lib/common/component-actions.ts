export type ComponentAction = (formData: FormData) => Promise<Response | void>;

export type ComponentActions = {
  [actionNames: string]: ComponentAction;
};

export async function applyActions(
  data: FormData,
  ...actions: ComponentActions[]
): Promise<Response | void> {
  const mergedActions = actions.reduce((acc, curr) => ({ ...acc, ...curr }), {});

  const actionName = data.get("actionName") as string;
  if (!actionName) return;
  data.delete("actionName");

  const action = mergedActions[actionName];
  if (!action) return;

  return await action(data);
}
