/**
 * Deep equality function with some caveats.
 *
 * Ordering of keys or values must be the same.
 */
export function simpleDeepEquals(a: unknown, b: unknown): boolean {
  return JSON.stringify(a) === JSON.stringify(b);
}
