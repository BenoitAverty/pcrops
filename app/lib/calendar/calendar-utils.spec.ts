import { makeMonths, makeWeeks } from "./calendar-utils";

describe("makeWeeks", () => {
  test("Full year", () => {
    const expected = [...Array(53).keys()].map((n) => ({ year: 2022, number: n }));
    expected[0] = { year: 2021, number: 52 }; // 2022 started during week 52 of 2021
    // expected = [52, 1, 2, ..., 52]
    expect(makeWeeks(2022, 0, 12)).toEqual(expected);
  });

  test("Full year with 53 weeks", () => {
    const expected = [...Array(53).keys()]
      .map((i) => i + 1)
      .map((n) => ({ year: 2026, number: n }));
    // expected = [1, 2, ..., 53]

    expect(makeWeeks(2026, 0, 12)).toEqual(expected);
  });

  test("Smaller period", () => {
    const expected = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22].map((n) => ({
      year: 2023,
      number: n,
    }));
    expect(makeWeeks(2023, 2, 3)).toEqual(expected);
  });

  test("Across two years", () => {
    const expected = [
      { year: 2023, number: 44 },
      { year: 2023, number: 45 },
      { year: 2023, number: 46 },
      { year: 2023, number: 47 },
      { year: 2023, number: 48 },
      { year: 2023, number: 49 },
      { year: 2023, number: 50 },
      { year: 2023, number: 51 },
      { year: 2023, number: 52 },
      { year: 2024, number: 1 },
      { year: 2024, number: 2 },
      { year: 2024, number: 3 },
      { year: 2024, number: 4 },
      { year: 2024, number: 5 },
      { year: 2024, number: 6 },
      { year: 2024, number: 7 },
      { year: 2024, number: 8 },
      { year: 2024, number: 9 },
    ];

    expect(makeWeeks(2023, 10, 4)).toEqual(expected);
  });
});

describe("makeMonths", () => {
  test("Full year", () => {
    const expected = [
      { name: "01", colspan: 16 },
      { name: "02", colspan: 12 },
      { name: "03", colspan: 13 },
      { name: "04", colspan: 12 },
      { name: "05", colspan: 14 },
      { name: "06", colspan: 13 },
      { name: "07", colspan: 13 },
      { name: "08", colspan: 13 },
      { name: "09", colspan: 13 },
      { name: "10", colspan: 14 },
      { name: "11", colspan: 12 },
      { name: "12", colspan: 14 },
    ];

    const actual = makeMonths(2022, 0, 12, (a) => a);
    expect(actual).toEqual(expected);

    // The sum of all colspans must equal 3 times the number of weeks, else the table is broken.
    expect(actual.map((m) => m.colspan).reduce((acc, curr) => acc + curr)).toEqual(
      makeWeeks(2022, 0, 12).length * 3,
    );
  });

  test("Full year with 53 weeks", () => {
    const expected = [
      { name: "01", colspan: 14 },
      { name: "02", colspan: 12 },
      { name: "03", colspan: 14 },
      { name: "04", colspan: 13 },
      { name: "05", colspan: 13 },
      { name: "06", colspan: 13 },
      { name: "07", colspan: 13 },
      { name: "08", colspan: 14 },
      { name: "09", colspan: 12 },
      { name: "10", colspan: 13 },
      { name: "11", colspan: 14 },
      { name: "12", colspan: 14 },
    ];

    const actual = makeMonths(2026, 0, 12, (a) => a);
    expect(actual).toEqual(expected);

    // The sum of all colspans must equal 3 times the number of weeks, else the table is broken.
    expect(actual.map((m) => m.colspan).reduce((acc, curr) => acc + curr)).toEqual(
      makeWeeks(2026, 0, 12).length * 3,
    );
  });

  test("Smaller period", () => {
    const expected = [
      { name: "03", colspan: 14 },
      { name: "04", colspan: 13 },
      { name: "05", colspan: 15 },
    ];
    const actual = makeMonths(2023, 2, 3, (a) => a);
    expect(actual).toEqual(expected);

    // The sum of all colspans must equal 3 times the number of weeks, else the table is broken.
    expect(actual.map((m) => m.colspan).reduce((acc, curr) => acc + curr)).toEqual(
      makeWeeks(2023, 2, 3).length * 3,
    );
  });

  test("Across two years", () => {
    const expected = [
      { name: "11", colspan: 14 },
      { name: "12", colspan: 13 },
      { name: "01", colspan: 13 },
      { name: "02", colspan: 14 },
    ];

    expect(makeMonths(2023, 10, 4, (a) => a)).toEqual(expected);
  });
});
