import { difference, w } from "./weeks";

describe("difference", () => {
  test("Basic case (same year)", () => {
    expect(difference(w(20, 2023), w(12, 2023))).toEqual(8);
  });
  test("Negative case (same year)", () => {
    expect(difference(w(20, 2023), w(30, 2023))).toEqual(-10);
  });
  test("across two years", () => {
    expect(difference(w(10, 2024), w(51, 2023))).toEqual(11);
  });
  test("Negative across two years", () => {
    expect(difference(w(40, 2023), w(5, 2024))).toEqual(-17);
  });
  test("53-week year", () => {
    expect(difference(w(1, 2027), w(50, 2026))).toEqual(4);
  });
  test("53-week year (negative)", () => {
    expect(difference(w(49, 2026), w(2, 2027))).toEqual(-6);
  });
  test("No support for big difference (more than a year)", () => {
    expect(() => difference(w(1, 2022), w(1, 2024))).toThrow("Big differences not supported");
  });
});
