import assert from "assert";

import { TFunction } from "i18next";

import { Week, generateWeeksBetween, getDayStartingMonday, getWeek } from "./weeks";

export const DEFAULT_YEAR = 2023; // TODO : change this to 2023 and then get rid of it

// This corresponds to the parameters of the makeWeeks/makeMonths functions.
export type CalendarPeriod = {
  startingYear: number;
  startingMonth: number;
  durationInMonths: number;
};

export const monthNameIntl = (t: TFunction) => (monthNum: string) => {
  const month = t("monthHeading", {
    val: new Date(`2000-${monthNum}-01`),
    formatParams: {
      val: { month: "long" },
    },
  });
  return month.length > 4 ? month.substring(0, 3) + "." : month;
};

/**
 * Returns the array of all weeks between the first of the given month and the  last day after a given
 * period in months.
 *
 * @param startingYear Year of the first month
 * @param startingMonth Staring month (January = 0, December = 11)
 * @param durationInMonths duration of the period in months. The period always ends at the end of the month.
 */
export function makeWeeks(
  startingYear: number,
  startingMonth: number,
  durationInMonths: number,
): Week[] {
  assert(
    durationInMonths >= 3 && durationInMonths <= 12,
    "The calendar can only display between 3 months and a year",
  );

  const startingDate = new Date(startingYear, startingMonth, 1, 0, 0, 0, 0);
  const endDate = new Date(startingYear, startingMonth + durationInMonths, 0, 0, 0, 0, 0);
  return [...generateWeeksBetween(startingDate, endDate)];
}

export function makeMonths(
  startingYear: number,
  startingMonth: number,
  durationInMonths: number,
  monthName: (_: string) => string,
) {
  assert(
    durationInMonths >= 3 && durationInMonths <= 12,
    "The calendar can only display between 3 months and a year",
  );

  const startingDate = new Date(startingYear, startingMonth, 1, 0, 0, 0, 0);
  return [...generateMonths(startingDate, durationInMonths, monthName)];
}

function* generateMonths(
  startingDate: Date,
  durationInMonth: number,
  monthName: (_: string) => string,
) {
  let current = startingDate;

  for (let i = 0; i < durationInMonth; ++i) {
    const firstDayOfMonth = new Date(current.getFullYear(), current.getMonth(), 1);
    const lastDayOfMonth = new Date(current.getFullYear(), current.getMonth() + 1, 0);

    const firstWeek = getWeek(firstDayOfMonth).number;
    const endWeek = getWeek(lastDayOfMonth).number;

    // This counts the number of weeks that are completely in the current month
    let fullWeeks = 0;
    // small half week is a week that is partially in the month with most of its days in another month
    // long half week is a week that is partially in the month with most of its days in the current
    let smallHalfWeeks = 0,
      longHalfWeeks = 0;

    if (firstWeek === 52 || firstWeek === 53) {
      // January only. The year starts in the last week of the previous year
      fullWeeks = endWeek - 1;
    } else if (endWeek == 1) {
      // December only. The year ends in the first week of the next year.
      fullWeeks = 52 - firstWeek; // Can't have 53 weeks in that case.
    } else {
      fullWeeks = endWeek - firstWeek - 1;
    }

    const dayOfWeekFirstDayOfMonth = getDayStartingMonday(firstDayOfMonth);
    const dayOfWeekLastDayOfMonth = getDayStartingMonday(lastDayOfMonth);

    // Look at the first week.
    if (dayOfWeekFirstDayOfMonth === 0 || i === 0) fullWeeks += 1;
    else if (dayOfWeekFirstDayOfMonth > 3) smallHalfWeeks += 1;
    else if (dayOfWeekFirstDayOfMonth <= 3) longHalfWeeks += 1;

    // And then the last week
    if (dayOfWeekLastDayOfMonth === 6 || i === durationInMonth - 1) fullWeeks += 1;
    else if (dayOfWeekLastDayOfMonth >= 3) longHalfWeeks += 1;
    else if (dayOfWeekLastDayOfMonth < 3) smallHalfWeeks += 1;

    yield {
      name: monthName((firstDayOfMonth.getMonth() + 1).toString().padStart(2, "0")),
      colspan: 3 * fullWeeks + 2 * longHalfWeeks + 1 * smallHalfWeeks,
    };

    current = new Date(lastDayOfMonth);
    current.setDate(current.getDate() + 2);
  }
}
