/**
 * This file contains the Week type and some sort of an "arithmetic" of it. a week is the base unit
 * of time in the app.
 */
export type Week = {
  year: number; // Year of this week
  number: number; // Week number between 1 and 53
};

/**
 * make a Week object from a number and a year
 */
export function w(number: number, year: number) {
  return {
    year,
    number,
  };
}

/**
 * returns
 *  - negative integer if w1 is before w2
 *  - 0 if they are the same week
 *  - positive integer if w1 is after w2
 */
export function compareWeeks(w1: Week, w2: Week) {
  // This only works after the year 53.
  return w1.year * 53 + w1.number - (w2.year * 53 + w2.number);
}

/**
 * Shift a week by a certain number of weeks. The shift amount can be negative to get an earlier week.
 */
export function shiftWeek(week: Week, amount: number): Week {
  // TODO refactor this method. It can probably be more efficient.

  // find any day in the given week. Works because january 4th is always in week 1
  const dayInWeek = new Date(week.year, 0, 4 + 7 * (week.number - 1));
  if (amount >= 0) {
    const weeks = [...generateNWeeksForward(dayInWeek, amount + 1)];
    return weeks[weeks.length - 1];
  } else {
    const weeks = [...generateNWeeksBackward(dayInWeek, -amount + 1)];
    return weeks[weeks.length - 1];
  }
}

/**
 * Find the difference between two weeks.
 *
 * equivalent to how many weeks you need to shift "w2" to arrive at "w1"
 * @param w1 the first week
 * @param w2 the second week
 * @return an integer. Positive if w2 is before w1
 */
export function difference(w1: Week, w2: Week): number {
  if (w1.year === w2.year) {
    return w1.number - w2.number;
  } else if (w1.year + 1 === w2.year) {
    const modulus = getLastWeek(w1.year).number;
    return w1.number - w2.number - modulus;
  } else if (w2.year + 1 === w1.year) {
    const modulus = getLastWeek(w2.year).number;
    return w1.number - w2.number + modulus;
  } else {
    throw new Error("Big differences not supported");
  }
}

/**
 * Returns the latter of two weeks
 */
export function max(w1: Week, w2: Week) {
  return compareWeeks(w1, w2) >= 0 ? w1 : w2;
}

/**
 * Returns the earlier of two weeks
 */
export function min(w1: Week, w2: Week) {
  return compareWeeks(w1, w2) <= 0 ? w1 : w2;
}

export function* generateWeeksBetween(startingDate: Date, endDate: Date) {
  const current = startingDate;
  while (current <= endDate) {
    yield getWeek(current);
    current.setDate(current.getDate() + 7);
  }
}

function* generateNWeeksForward(startingDate: Date, n: number) {
  const current = startingDate;
  for (let i = 0; i < n; i++) {
    yield getWeek(current);
    current.setDate(current.getDate() + 7);
  }
}

function* generateNWeeksBackward(startingDate: Date, n: number) {
  const current = startingDate;
  for (let i = 0; i < n; i++) {
    yield getWeek(current);
    current.setDate(current.getDate() - 7);
  }
}

export function getWeek(date: Date): Week {
  const thursdayOfWeek = new Date(date.getTime());
  thursdayOfWeek.setHours(0, 0, 0, 0);
  // Shift the date to the thursday of this week
  thursdayOfWeek.setDate(thursdayOfWeek.getDate() - getDayStartingMonday(thursdayOfWeek) + 3);

  // Take the date object for January the 4th (always in week 1) then do the same thing to get the first thursday of the year
  const firstThursday = new Date(thursdayOfWeek.getFullYear(), 0, 4);
  firstThursday.setDate(firstThursday.getDate() - getDayStartingMonday(firstThursday) + 3);

  return {
    year: thursdayOfWeek.getFullYear(),
    // Count the number of weeks between the first thursday and this week's thursday.
    number:
      1 + Math.round((thursdayOfWeek.getTime() - firstThursday.getTime()) / (7 * 24 * 3600000)),
  };
}

/**
 * returns the last week in a year
 * @param year the year number
 */
function getLastWeek(year: number) {
  return getWeek(new Date(year, 11, 31));
}

// I can't reason with a week that starts on sunday.
export function getDayStartingMonday(date: Date) {
  return (date.getDay() + 6) % 7;
}
