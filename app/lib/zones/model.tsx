import { GardenZone } from "@prisma/client";
import invariant from "tiny-invariant";

import { Detached } from "~/lib/common/entity";
import { DetachedZoneCrop } from "~/lib/crops/model";

export function reorder<T extends Detached<GardenZone>>(
  zones: T[],
  sourceIndex: number,
  destinationIndex: number,
): T[] {
  invariant(sourceIndex in zones);
  invariant(destinationIndex in zones);

  const result = [...zones];
  const [removed] = result.splice(sourceIndex, 1);
  result.splice(destinationIndex, 0, removed);

  return result.map((zone, newIndex) => ({
    ...zone,
    order: newIndex,
  }));
}

export type GroupedCrops<T extends DetachedZoneCrop> = {
  /** List of crops that are not assigned to a zone */
  noZone: T[];
  /** List of zone names ordered by zone order */
  orderedZonesKeys: string[];
  /** a dictionary of zone name to zones and crops that are assigned to this zone */
  zones: { [zoneName: string]: { zone: T["zone"]; crops: T[] } };
};

/**
 * Assign the given crops to their zone. The given zones are used to include the zones that have no crops
 *
 * Assumption : All zones and crops have the same owner.
 */
export function groupByZone<T extends DetachedZoneCrop>(
  crops: T[],
  zones: Detached<GardenZone>[],
): GroupedCrops<T> {
  const cropsWithNoZone = crops.filter((c) => c.zone === null);
  const cropsWithZone = crops.filter((c) => c.zone !== null);
  const orderedZonesKeys = zones
    .sort((z1, z2) => z1.order - z2.order)
    .reduce((set, z) => set.add(z.name), new Set<string>());

  return {
    noZone: cropsWithNoZone,
    orderedZonesKeys: [...orderedZonesKeys],
    zones: zones
      .map((zone) => ({
        zone,
        crops: cropsWithZone.filter((c) => c.zone?.name === zone.name),
      }))
      .reduce(
        (acc, curr) => ({
          ...acc,
          [curr.zone.name]: curr,
        }),
        {},
      ),
  };
}
