import { DetachedZoneCrop } from "../crops/model";

import { groupByZone, GroupedCrops } from "./model";

describe("groupByZone", () => {
  test("Grouping crops", () => {
    const zones = [
      {
        tid: "a",
        name: "Zone A",
        description: null,
        order: 1,
        ownerTid: "a",
      },
      {
        tid: "b",
        name: "Zone B",
        description: null,
        order: 0,
        ownerTid: "a",
      },
    ];
    const cropBase = {
      ownerTid: "a",
      variety: null,
    };
    const crops: DetachedZoneCrop[] = [
      {
        ...cropBase,
        species: "Tomato",
        zone: zones[0],
        zoneTid: zones[0].tid,
      },
      {
        ...cropBase,
        species: "Cucumber",
        zoneTid: zones[0].tid,
        zone: zones[0],
      },
      {
        ...cropBase,
        species: "Beans",
        zoneTid: zones[1].tid,
        zone: zones[1],
      },
      {
        ...cropBase,
        species: "Eggplant",
        zoneTid: zones[1].tid,
        zone: zones[1],
      },
      {
        ...cropBase,
        species: "Cauliflower",
        zoneTid: null,
        zone: null,
      },
    ];

    const expected: GroupedCrops<DetachedZoneCrop> = {
      noZone: [crops[4]],
      orderedZonesKeys: ["Zone B", "Zone A"],
      zones: {
        [zones[0].name]: {
          zone: zones[0],
          crops: [crops[0], crops[1]],
        },
        [zones[1].name]: {
          zone: zones[1],
          crops: [crops[2], crops[3]],
        },
      },
    };

    expect(groupByZone(crops, zones)).toEqual(expected);
  });
});
