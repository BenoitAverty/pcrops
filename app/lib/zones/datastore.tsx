import { GardenZone } from "@prisma/client";

import { prisma } from "~/lib/common/prisma.server";

/**
 * List zones of the given user.
 */
export function findByOwnerId(userTid: string) {
  return prisma.gardenZone.findMany({
    where: {
      ownerTid: userTid,
    },
    orderBy: [
      {
        order: "asc",
      },
      {
        createdAt: "asc",
      },
    ],
  });
}

/**
 * Create a zone for the given user
 */
export async function create(userTid: string, zoneName: string, zoneDesc?: string | null) {
  await prisma.$transaction(async (prisma) => {
    const {
      _max: { order },
    } = await prisma.gardenZone.aggregate({
      _max: {
        order: true,
      },
    });
    await prisma.gardenZone.create({
      data: {
        ownerTid: userTid,
        name: zoneName,
        description: zoneDesc,
        order: order !== null ? order + 1 : 0,
      },
    });
  });
}

/**
 * Drop the given zone
 */
export async function drop(zoneTid: string) {
  await prisma.gardenZone.delete({
    where: {
      tid: zoneTid,
    },
  });
}

/**
 * update given zones
 */
export async function updateMany(zones: GardenZone[]) {
  await prisma.$transaction(
    zones.map((z) =>
      prisma.gardenZone.update({
        where: { tid: z.tid },
        data: z,
      }),
    ),
  );
}
