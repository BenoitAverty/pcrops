import { DetachedPlannedCrop, FullCrop, getEndWeek, getStartWeek, PlannedCrop } from "./model";

import { prisma } from "~/lib/common/prisma.server";
import { compareWeeks, min } from "~/lib/calendar/weeks";
import { CalendarPeriod, makeWeeks } from "~/lib/calendar/calendar-utils";

/**
 * List crops of the given user.
 * Only crops with at least one period of the plan included in the given calendar period are returned
 */
export async function findByOwnerId(userTid: string, period: CalendarPeriod): Promise<FullCrop[]> {
  // TODO find a way to do that using prisma, or raise them an issue.
  function sortCrops(crops: FullCrop[]) {
    const minStartWeek = (c: FullCrop) =>
      c.plan.map((p) => getStartWeek(p)).reduce((earliestWeek, curr) => min(earliestWeek, curr));

    return [...crops].sort((c1, c2) => compareWeeks(minStartWeek(c1), minStartWeek(c2)));
  }

  const weeks = makeWeeks(period.startingYear, period.startingMonth, period.durationInMonths);
  const firstWeek = weeks[0];
  const lastWeek = weeks[weeks.length - 1];

  function filterCropsByPeriod(crops: FullCrop[]) {
    return crops.filter(
      (crop) =>
        crop.plan.findIndex(
          (cropPeriod) =>
            compareWeeks(getStartWeek(cropPeriod), firstWeek) >= 0 &&
            compareWeeks(getEndWeek(cropPeriod), lastWeek) <= 0,
        ) >= 0,
    );
  }

  const dbResult = await prisma.crop.findMany({
    where: {
      ownerTid: userTid,
      plan: {
        // Filter crops that spans over the period year. Filtering by week is done in JS later
        // TODO : completely filter in the DB (probably involves a raw query)
        some: {
          startYear: { gte: firstWeek.year },
          endYear: { lte: lastWeek.year },
        },
      },
    },
    orderBy: {
      createdAt: "asc",
    },
    include: {
      plan: true,
      zone: true,
    },
  });

  return sortCrops(filterCropsByPeriod(dbResult));
}

/**
 * Save a crop for the given user
 */
export async function create(crop: DetachedPlannedCrop) {
  await prisma.crop.create({ data: { ...crop, plan: { create: crop.plan } } });
}

/**
 * Update a crop entity
 */
export async function update(crop: Partial<PlannedCrop>) {
  await prisma.crop.update({
    where: { tid: crop.tid },
    data: {
      ...crop,
      plan: crop.plan && {
        deleteMany: {},
        createMany: {
          data: crop.plan.map(({ cropTid: _, ...period }) => period),
        },
      },
    },
  });
}

export async function drop(tid: string) {
  await prisma.crop.delete({
    where: { tid },
  });
}

export async function assignZone(cropTid: string, targetZoneTid: string | null) {
  await prisma.crop.update({
    where: { tid: cropTid },
    data: {
      zoneTid: targetZoneTid,
    },
  });
}
