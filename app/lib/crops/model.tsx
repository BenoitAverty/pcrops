import { Crop, CropPeriod, CropState, GardenZone, Prisma } from "@prisma/client";

import { Detached } from "../common/entity";
import { DEFAULT_YEAR } from "../calendar/calendar-utils";
import { compareWeeks, difference, max, min, shiftWeek, w, Week } from "../calendar/weeks";

export type OrphanCropPeriod = Omit<CropPeriod, "cropTid">;
export type DetachedPlannedCrop = Detached<Crop> & { plan: OrphanCropPeriod[] };
export type DetachedZoneCrop = Detached<Crop> & { zone: Detached<GardenZone> | null };
const plannedCrop = Prisma.validator<Prisma.CropArgs>()({
  include: { plan: true },
});
export type PlannedCrop = Prisma.CropGetPayload<typeof plannedCrop>;
const zoneCrop = Prisma.validator<Prisma.CropArgs>()({
  include: { zone: true },
});
export type ZoneCrop = Prisma.CropGetPayload<typeof zoneCrop>;
export type FullCrop = PlannedCrop & ZoneCrop;

export function period(
  startWeek: number,
  startYear: number,
  endWeek: number,
  endYear: number,
  state: CropState,
): OrphanCropPeriod {
  return {
    startWeek,
    startYear,
    endWeek,
    endYear,
    state,
  };
}

export function defaultCrop(
  ownerTid: string,
  speciesName: string,
  sowing = false,
  planting = false,
): DetachedPlannedCrop {
  const plan = [];
  if (sowing && !planting) {
    plan.push(
      period(10, DEFAULT_YEAR, 10, DEFAULT_YEAR, CropState.SOWING),
      period(11, DEFAULT_YEAR, 30, DEFAULT_YEAR, CropState.GROWTH),
    );
  } else if (!sowing && planting) {
    plan.push(
      period(15, DEFAULT_YEAR, 15, DEFAULT_YEAR, CropState.PLANTING),
      period(16, DEFAULT_YEAR, 30, DEFAULT_YEAR, CropState.GROWTH),
    );
  } else if (sowing && planting) {
    plan.push(
      period(10, DEFAULT_YEAR, 10, DEFAULT_YEAR, CropState.SOWING),
      period(11, DEFAULT_YEAR, 14, DEFAULT_YEAR, CropState.GROWTH),
      period(15, DEFAULT_YEAR, 15, DEFAULT_YEAR, CropState.PLANTING),
      period(16, DEFAULT_YEAR, 30, DEFAULT_YEAR, CropState.GROWTH),
    );
  } else {
    throw new Error(
      "Cannot create a crop without sowing or planting. Plants do not come from nowhere !",
      // TODO actually... should we support spontaneous plants ?
    );
  }

  return {
    ownerTid,
    species: speciesName,
    variety: null,
    zoneTid: null,
    plan: [...plan, period(31, DEFAULT_YEAR, 35, DEFAULT_YEAR, CropState.HARVEST)],
  };
}

export function periodInclude(period: OrphanCropPeriod, week: Week) {
  const periodStartWeek = { year: period.startYear, number: period.startWeek };
  const periodEndWeek = { year: period.endYear, number: period.endWeek };

  const result = compareWeeks(periodStartWeek, week) <= 0 && compareWeeks(week, periodEndWeek) <= 0;
  return result;
}

/**
 * Return the period in the plan that correspond to the given week
 */
export function currentPeriodIndex(plan: OrphanCropPeriod[], week: Week): number {
  return plan.findIndex((p) => periodInclude(p, week));
}

// TODO : refactor this, maybe introducing "resize left" and "resize right" actions.
export function computeNewPeriod(
  plan: OrphanCropPeriod[],
  action: "resize" | "move",
  from: Week,
  to: Week,
): OrphanCropPeriod[] {
  const sortedPlan = plan
    .map((p) => ({ ...p }))
    .sort((a, b) => compareWeeks(w(a.startWeek, a.startYear), w(b.endWeek, b.endYear)));
  const changedPeriodIndex = currentPeriodIndex(sortedPlan, from);
  if (!sortedPlan[changedPeriodIndex]) return plan;

  const changedPeriod = sortedPlan[changedPeriodIndex];
  const nextPeriod = sortedPlan[changedPeriodIndex + 1];
  const prevPeriod = sortedPlan[changedPeriodIndex - 1];

  const nextPeriodIsAdjacent =
    nextPeriod &&
    compareWeeks(shiftWeek(getEndWeek(changedPeriod), 1), getStartWeek(nextPeriod)) === 0;
  const prevPeriodIsAdjacent =
    prevPeriod &&
    compareWeeks(shiftWeek(getEndWeek(prevPeriod), 1), getStartWeek(changedPeriod)) === 0;

  if (action === "move") {
    const movement = difference(to, from);

    shiftStart(changedPeriod, movement);
    shiftEnd(changedPeriod, movement);

    if (nextPeriod) {
      if (compareWeeks(from, to) < 0) {
        const newStart = max(shiftWeek(getEndWeek(changedPeriod), 1), getStartWeek(nextPeriod));
        setStartWeek(nextPeriod, newStart);
      } else if (nextPeriodIsAdjacent) {
        setStartWeek(nextPeriod, shiftWeek(getEndWeek(changedPeriod), 1));
      }
      setEndWeek(nextPeriod, max(getStartWeek(nextPeriod), getEndWeek(nextPeriod)));
    }
    if (prevPeriod) {
      if (compareWeeks(from, to) > 0) {
        const newEnd = min(shiftWeek(getStartWeek(changedPeriod), -1), getEndWeek(prevPeriod));
        setEndWeek(prevPeriod, newEnd);
      } else if (prevPeriodIsAdjacent) {
        setEndWeek(prevPeriod, shiftWeek(getStartWeek(changedPeriod), -1));
      }
      setStartWeek(prevPeriod, min(getEndWeek(prevPeriod), getStartWeek(prevPeriod)));
    }
  }
  if (action === "resize") {
    const toIsInside = periodInclude(changedPeriod, to);
    if (toIsInside && compareWeeks(from, to) < 0) {
      setStartWeek(changedPeriod, to);
      if (prevPeriod) {
        setEndWeek(prevPeriod, shiftWeek(to, -1));
      }
    } else if (!toIsInside && compareWeeks(from, to) > 0) {
      setStartWeek(changedPeriod, to);
      if (prevPeriod) {
        setEndWeek(prevPeriod, min(getEndWeek(prevPeriod), shiftWeek(to, -1)));
        setStartWeek(prevPeriod, min(getEndWeek(prevPeriod), getStartWeek(prevPeriod)));
      }
    } else if (toIsInside && compareWeeks(from, to) > 0) {
      setEndWeek(changedPeriod, to);
      if (nextPeriod) {
        setStartWeek(nextPeriod, shiftWeek(to, 1));
      }
    } else if (!toIsInside && compareWeeks(from, to) < 0) {
      setEndWeek(changedPeriod, to);
      if (nextPeriod) {
        setStartWeek(nextPeriod, max(getStartWeek(nextPeriod), shiftWeek(to, 1)));
        setEndWeek(nextPeriod, max(getStartWeek(nextPeriod), getEndWeek(nextPeriod)));
      }
    }
  }

  return sortedPlan;
}

/**
 * Shift the end week of the period
 *
 * @see shiftWeek
 */
function shiftStart(period: OrphanCropPeriod, n: number) {
  const periodStart = w(period.startWeek, period.startYear);
  const newStart = shiftWeek(periodStart, n);
  period.startWeek = newStart.number;
  period.startYear = newStart.year;
}

/**
 * Shift the start week of the period
 *
 * @see shiftWeek
 */
function shiftEnd(period: OrphanCropPeriod, n: number) {
  const periodEnd = w(period.endWeek, period.endYear);
  const newEnd = shiftWeek(periodEnd, n);
  period.endWeek = newEnd.number;
  period.endYear = newEnd.year;
}

/**
 * Get the start of the period as a Week object
 */
export function getStartWeek(period: OrphanCropPeriod) {
  return w(period.startWeek, period.startYear);
}

/**
 * Set the start of the period as a Week object
 */
function setStartWeek(period: OrphanCropPeriod, week: Week) {
  period.startWeek = week.number;
  period.startYear = week.year;
}

/**
 * Get the end of the period as a Week object
 */
export function getEndWeek(period: OrphanCropPeriod) {
  return w(period.endWeek, period.endYear);
}

/**
 * Set the end of the period as a Week object
 */
function setEndWeek(period: OrphanCropPeriod, week: Week) {
  period.endWeek = week.number;
  period.endYear = week.year;
}

export function periodLength(currentPeriod: OrphanCropPeriod) {
  return difference(getEndWeek(currentPeriod), getStartWeek(currentPeriod)) + 1;
}
