import { CropState } from "@prisma/client";

import { w, Week } from "../calendar/weeks";

import { computeNewPeriod, defaultCrop, DetachedPlannedCrop, period } from "./model";

describe("defaultCrop", () => {
  test.each([
    {
      species: "Tomato",
      sowing: false,
      planting: true,
      expectedPlan: [
        period(15, 2023, 15, 2023, CropState.PLANTING),
        period(16, 2023, 30, 2023, CropState.GROWTH),
        period(31, 2023, 35, 2023, CropState.HARVEST),
      ],
    },
    {
      species: "Beans",
      sowing: true,
      planting: false,
      expectedPlan: [
        period(10, 2023, 10, 2023, CropState.SOWING),
        period(11, 2023, 30, 2023, CropState.GROWTH),
        period(31, 2023, 35, 2023, CropState.HARVEST),
      ],
    },
    {
      species: "Cucumbers",
      sowing: true,
      planting: true,
      expectedPlan: [
        period(10, 2023, 10, 2023, CropState.SOWING),
        period(11, 2023, 14, 2023, CropState.GROWTH),
        period(15, 2023, 15, 2023, CropState.PLANTING),
        period(16, 2023, 30, 2023, CropState.GROWTH),
        period(31, 2023, 35, 2023, CropState.HARVEST),
      ],
    },
  ])(
    "$species, sowing : $sowing, planting: $planting",
    ({ species, sowing, planting, expectedPlan }) => {
      const actual = defaultCrop("a", species, sowing, planting);
      const expected: DetachedPlannedCrop = {
        species,
        variety: null,
        ownerTid: "a",
        zoneTid: null,
        plan: expectedPlan,
      };

      expect(actual).toEqual(expected);
    },
  );

  test("throwing an error if illegal state", () => {
    expect(() => defaultCrop("Any", "Any", false, false)).toThrowError();
  });
});

describe("computeNewPeriod", () => {
  test.each([
    [
      "Moving a non existent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      ["move", w(10, 2022), w(5, 2022)],
    ],
    [
      "Resizing a period",
      [period(15, 2022, 20, 2022, CropState.PLANTING)],
      [period(18, 2022, 20, 2022, CropState.PLANTING)],
      ["resize", w(15, 2022), w(18, 2022)],
    ],
    [
      "moving a period and resizing the next adjacent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(13, 2022, 13, 2022, CropState.PLANTING),
        period(14, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      ["move", w(15, 2022), w(13, 2022)],
    ],
    [
      "moving a period and resizing the previous adjacent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 25, 2022, CropState.GROWTH),
        period(26, 2022, 30, 2022, CropState.HARVEST),
      ],
      ["move", w(33, 2022), w(28, 2022)],
    ],
    [
      "growing a period and resizing the previous adjacent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 25, 2022, CropState.GROWTH),
        period(26, 2022, 35, 2022, CropState.HARVEST),
      ],
      ["resize", w(31, 2022), w(26, 2022)],
    ],
    [
      "shrinking a period and resizing the previous adjacent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 19, 2022, CropState.PLANTING),
        period(20, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      ["resize", w(16, 2022), w(20, 2022)],
    ],
    [
      "growing a period and resizing the next adjacent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 16, 2022, CropState.PLANTING),
        period(17, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      ["resize", w(15, 2022), w(16, 2022)],
    ],
    [
      "shrinking a period and resizing the next adjacent period",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 35, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 25, 2022, CropState.GROWTH),
        period(26, 2022, 35, 2022, CropState.HARVEST),
      ],
      ["resize", w(30, 2022), w(25, 2022)],
    ],
    [
      "resizing a period and not resizing the next period if it is not adjacent",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 20, 2022, CropState.GROWTH),
        period(21, 2022, 21, 2022, CropState.HARVEST),
        period(25, 2022, 25, 2022, CropState.PLANTING),
        period(26, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 31, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 20, 2022, CropState.GROWTH),
        period(21, 2022, 23, 2022, CropState.HARVEST),
        period(25, 2022, 25, 2022, CropState.PLANTING),
        period(26, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 31, 2022, CropState.HARVEST),
      ],
      ["resize", w(21, 2022), w(23, 2022)],
    ],
    [
      "moving a period to the right and not resizing the next period if it is not adjacent",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 20, 2022, CropState.GROWTH),
        period(21, 2022, 21, 2022, CropState.HARVEST),
        period(25, 2022, 25, 2022, CropState.PLANTING),
        period(26, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 31, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 22, 2022, CropState.GROWTH),
        period(23, 2022, 23, 2022, CropState.HARVEST),
        period(25, 2022, 25, 2022, CropState.PLANTING),
        period(26, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 31, 2022, CropState.HARVEST),
      ],
      ["move", w(21, 2022), w(23, 2022)],
    ],
    [
      "moving a period to the left and not resizing the next period if it is not adjacent",
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 20, 2022, CropState.GROWTH),
        period(21, 2022, 21, 2022, CropState.HARVEST),
        period(25, 2022, 25, 2022, CropState.PLANTING),
        period(26, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 31, 2022, CropState.HARVEST),
      ],
      [
        period(15, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 20, 2022, CropState.GROWTH),
        period(21, 2022, 21, 2022, CropState.HARVEST),
        period(23, 2022, 23, 2022, CropState.PLANTING),
        period(24, 2022, 30, 2022, CropState.GROWTH),
        period(31, 2022, 31, 2022, CropState.HARVEST),
      ],
      ["move", w(25, 2022), w(23, 2022)],
    ],
    [
      "resizing a period and resizing a previous non-adjacent period that is not far enough",
      [
        period(10, 2022, 15, 2022, CropState.PLANTING),
        period(20, 2022, 25, 2022, CropState.GROWTH),
      ],
      [
        period(10, 2022, 12, 2022, CropState.PLANTING),
        period(13, 2022, 25, 2022, CropState.GROWTH),
      ],
      ["resize", w(20, 2022), w(13, 2022)],
    ],
    [
      "resizing a period and resizing a next non-adjacent period that is not far enough",
      [
        period(10, 2022, 15, 2022, CropState.PLANTING),
        period(20, 2022, 25, 2022, CropState.GROWTH),
        period(30, 2022, 40, 2022, CropState.HARVEST),
      ],
      [
        period(10, 2022, 15, 2022, CropState.PLANTING),
        period(20, 2022, 32, 2022, CropState.GROWTH),
        period(33, 2022, 40, 2022, CropState.HARVEST),
      ],
      ["resize", w(25, 2022), w(32, 2022)],
    ],
    [
      "moving far to the left resizes and move the prev period",
      [
        period(12, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 18, 2022, CropState.GROWTH),
        period(19, 2022, 20, 2022, CropState.HARVEST),
      ],
      [
        period(10, 2022, 10, 2022, CropState.PLANTING),
        period(11, 2022, 13, 2022, CropState.GROWTH),
        period(14, 2022, 20, 2022, CropState.HARVEST),
      ],
      ["move", w(17, 2022), w(12, 2022)],
    ],
    [
      "moving far to the right resizes and move the next period",
      [
        period(12, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 18, 2022, CropState.GROWTH),
        period(19, 2022, 20, 2022, CropState.HARVEST),
      ],
      [
        period(12, 2022, 18, 2022, CropState.PLANTING),
        period(19, 2022, 21, 2022, CropState.GROWTH),
        period(22, 2022, 22, 2022, CropState.HARVEST),
      ],
      ["move", w(17, 2022), w(20, 2022)],
    ],
    [
      "resizing far to the left resizes and move the prev period",
      [
        period(12, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 18, 2022, CropState.GROWTH),
        period(19, 2022, 20, 2022, CropState.HARVEST),
      ],
      [
        period(10, 2022, 10, 2022, CropState.PLANTING),
        period(11, 2022, 18, 2022, CropState.GROWTH),
        period(19, 2022, 20, 2022, CropState.HARVEST),
      ],
      ["resize", w(16, 2022), w(11, 2022)],
    ],
    [
      "resizing far to the right resizes and move the next period",
      [
        period(12, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 18, 2022, CropState.GROWTH),
        period(19, 2022, 20, 2022, CropState.HARVEST),
      ],
      [
        period(12, 2022, 15, 2022, CropState.PLANTING),
        period(16, 2022, 22, 2022, CropState.GROWTH),
        period(23, 2022, 23, 2022, CropState.HARVEST),
      ],
      ["resize", w(18, 2022), w(22, 2022)],
    ],
    // TODO : "cascading" operations. Resize a period, that makes the rev period move, but that period also makes its prev period move.
    // How to make it recursive maybe ?
  ])("%s", (_, inputPlan, expectedPlan, action) => {
    const actual = computeNewPeriod(
      inputPlan,
      action[0] as "move" | "resize",
      action[1] as Week,
      action[2] as Week,
    );

    expect(actual).toEqual(expectedPlan);
  });
});
