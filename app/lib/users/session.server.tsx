import * as process from "process";

import { createCookieSessionStorage } from "@remix-run/node";

let sessionSecret = process.env.SESSION_SECRET;
if (!sessionSecret) {
  console.warn("WARNING ! No session secret set. The sessions are not secure.");
  sessionSecret = "notsosecret";
}

const storage = createCookieSessionStorage({
  cookie: {
    name: "pcrops_session",
    secure: process.env.NODE_ENV === "production" && process.env.OVERRIDE_SECURE_COOKIE !== "true",
    secrets: [sessionSecret],
    sameSite: "lax",
    path: "/",
    maxAge: 60 * 60 * 24 * 180, // 180 days
    httpOnly: true,
  },
});

/**
 * Retrieves the session and read the user TID inside.
 *
 * If there is no session or it doesn't contain the tid, return null
 */
export async function getUserTid(request: Request): Promise<string | null> {
  const session = await storage.getSession(request.headers.get("Cookie"));
  const userTid = session.get("userTid");

  return userTid || null;
}

/**
 * Create a session and stores the user TID inside.
 *
 * Returns an object with the set-cookie header that must be returned to the browser
 */
export async function createUserSession(userTid: string): Promise<{ "Set-Cookie": string }> {
  const session = await storage.getSession();
  session.set("userTid", userTid);

  return {
    "Set-Cookie": await storage.commitSession(session),
  };
}

/**
 * Destroys the user session
 *
 * Returns an object with the set-cookie header that must be returned to the browser
 */
export async function destroyUserSession(): Promise<{ "Set-Cookie": string }> {
  const session = await storage.getSession();

  return {
    "Set-Cookie": await storage.destroySession(session),
  };
}
