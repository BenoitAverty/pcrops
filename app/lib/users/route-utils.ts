import { User } from "@prisma/client";
import { json, redirect } from "@remix-run/server-runtime";

import { createUserSession, getUserTid } from "./session.server";
import { changeTutorialStep, create, findByTid, saveUserActivity } from "./datastore";

/**
 * Get the current user from the DB after reading its tid in the session.
 *
 * If the user can't be found, then fail with a 401 status code.
 */
export async function requireUserOrFail(request: Request): Promise<User> {
  const userTid = await getUserTid(request);

  let user = null;
  if (userTid) {
    user = await findByTid(userTid);
  }

  if (!user) {
    throw json({ error: "You must be logged in." }, 401);
  }

  saveUserActivity(user.tid);
  return user;
}

/**
 * Get the current user from the DB after reading its tid in the session.
 *
 * If the user can't be found, return null.
 * @param request the request containing the user session
 */
export async function getUserOrNull(request: Request): Promise<User | null> {
  const userTid = await getUserTid(request);

  if (userTid === null) return null;

  const user = await findByTid(userTid);

  if (user) {
    saveUserActivity(user.tid);
  }

  return user;
}

/**
 * Get the current user from the DB after reading its tid in the session.
 *
 * If the user can't be found, then create one and redirect to the given path.
 * @param request the request containing the user session
 * @param redirectTo path to redirect to after user creation
 * @param userData data to apply to the created user (not applied if a user is already logged in)
 */
export async function requireUserOrCreate(
  request: Request,
  redirectTo: string,
  userData?: Partial<User>,
): Promise<User> {
  let user = await getUserOrNull(request);

  if (!user) {
    user = await create(userData);
    console.debug("created user : ", JSON.stringify(user));
    throw redirect(redirectTo, {
      headers: await createUserSession(user.tid),
    });
  }

  return user;
}

/**
 * Redirect the logged-in user to the tutorial step they're in. If they have not started the tutorial (step = 0),
 * set the user to step 1.
 *
 * if no redirect is needed, return the user.
 */
export async function requireUserAndRedirectToTutorialStep(request: Request): Promise<User> {
  const user = await requireUserOrCreate(request, "/tutorial/1", { tutorialStep: 1 });
  if (user.tutorialStep == 0) {
    await changeTutorialStep(user, 1);
  }
  const currentUrl = new URL(request.url).pathname;

  const targetUrl = user.tutorialStep > 0 ? `/tutorial/${user.tutorialStep}` : "/tutorial/1";

  if (currentUrl !== targetUrl) {
    throw redirect(targetUrl);
  } else {
    return user;
  }
}
