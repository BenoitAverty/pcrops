import { User } from "@prisma/client";
import { json } from "@remix-run/server-runtime";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime";

import { create, findByEmail, findByTid, updateUserEmail } from "~/lib/users/datastore";

/**
 * Returns true if the given user is registered, false otherwise
 * @param user
 */
export function isRegistered(user: User) {
  return user.email && user.email !== "";
}

/**
 * Register a user to a given email.
 *
 * If the userTid is null, this means the user must be created (already registered).
 * If the userTid doest't correspond to a user, that's a bug (shouldn't happen) -> 500 error
 * If the email is already used by another user, that means a user tried to register instead of login. Let the calling code handle that case.
 *
 * @param userTid the user that needs to be registered with the email. Null if the user must be created
 * @param email the email that will be registerred to the user, and that will be used to login afterwards
 * @return the user that has just been registered
 */
export async function registerUser(userTid: string | null, email: string): Promise<User> {
  if (userTid === null) {
    // If there is already a user with this email, log it in (no risk of losing data because the user is anonymous)
    const existingUser = await findByEmail(email);
    if (existingUser !== null) {
      return existingUser;
    } else {
      return create({ email });
    }
  } else {
    const user = await findByTid(userTid);
    if (!user) {
      console.error(
        `tid ${userTid} found in a register token doesn't correspond to an existing user`,
      );
      throw new Error("Trying to register an existing user but that user could not be found.");
    }

    if (user.email && user.email !== email) {
      throw json({ error: "That user is already registered." }, 400);
    }

    try {
      return updateUserEmail(user, email);
    } catch (e) {
      if (
        e instanceof PrismaClientKnownRequestError &&
        e.code === "P2002" /* unique constraint violation */
      ) {
        // In this case we can't just login the existing user because that would lose the data
        // of the current anonymous user.
        throw json({ error: "email already exists" }, 400);
      }
      throw e;
    }
  }
}

/**
 * Login a user given its email. If the user doesn't exist, that's an error (but the option can be given to the user to register instead)
 * @param email
 */
export async function loginUser(email: string): Promise<User> {
  const user = await findByEmail(email);

  if (user === null) {
    throw json({ error: "user doesn't exist" }, 401);
  }

  return user;
}
