import { User } from "@prisma/client";

import { prisma } from "../common/prisma.server";

export async function findByTid(tid: string): Promise<User | null> {
  const user = await prisma.user.findUnique({
    where: { tid },
  });
  return user;
}

export async function findByEmail(email: string): Promise<User | null> {
  const user = await prisma.user.findUnique({
    where: { email },
  });
  return user;
}

/**
 * Create a user in the DB and returns it
 * @param userData overrides the default values of the created user
 */
export async function create(userData?: Partial<User>): Promise<User> {
  return await prisma.user.create({
    data: {
      tutorialStep: 0,
      ...userData,
    },
  });
}

export async function changeTutorialStep(user: User, step: number) {
  return await prisma.user.update({
    where: { tid: user.tid },
    data: { tutorialStep: step },
  });
}

export async function updateUserEmail(user: User, email: string) {
  return await prisma.user.update({
    where: { tid: user.tid },
    data: { email },
  });
}

export function saveUserActivity(tid: string) {
  prisma.user
    .update({
      where: {
        tid,
      },
      data: {
        lastSeenAt: new Date(),
      },
    })
    .catch((e) => console.warn("Failed to save user activity", e));
}
