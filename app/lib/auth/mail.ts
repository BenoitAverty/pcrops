import { createTransport } from "nodemailer";

export async function sendRegisterToken(email: string, baseUri: string, token: string) {
  const subject = "Confirm your registration to pcrops.fly.dev";
  const text = `Confirm registration at ${baseUri}/magic/${token}`;

  await sendTheMail(email, subject, text);
}

export async function sendLoginToken(email: string, baseUri: string, token: string) {
  const subject = "Log-in to pcrops.fly.dev";
  const text = `Click the following link to log-in to pcrops.fly.dev : ${baseUri}/magic/${token}`;

  await sendTheMail(email, subject, text);
}

async function sendTheMail(email: string, subject: string, text: string) {
  const transporter = createTransport({
    host: process.env.SMTP_HOST,
    port: Number(process.env.SMTP_PORT),
    secure: process.env.SMTP_SECURE === "true",
    auth: process.env.SMTP_USER
      ? {
          user: process.env.SMTP_USER,
          pass: process.env.SMTP_PASSWORD,
        }
      : undefined,
  });
  const info = await transporter.sendMail({
    from: '"pcrops register" <register@pcrops.ab0.fr>',
    to: email,
    subject: subject,
    text: text,
  });

  console.debug(`sent mail`, JSON.stringify(info, null, 2));
}
