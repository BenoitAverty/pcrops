import { JwtPayload, sign, verify } from "jsonwebtoken";
import { json } from "@remix-run/server-runtime";
import invariant from "tiny-invariant";

export type AuthToken =
  | {
      type: "register";
      sub: string | null;
      email: string;
    }
  | {
      type: "login";
      sub: null;
      email: string;
    };

export function generateLoginToken(email: string) {
  invariant(process.env.JWT_SECRET, "JWT_SECRET env var not set.");

  const payload: AuthToken = {
    type: "login",
    sub: null,
    email,
  };

  return sign(payload, process.env.JWT_SECRET, {
    expiresIn: "1h",
  });
}

export function generateRegisterToken(email: string, userTid: string | null) {
  invariant(process.env.JWT_SECRET, "JWT_SECRET env var not set.");

  const payload: AuthToken = {
    type: "register",
    sub: userTid,
    email,
  };

  return sign(payload, process.env.JWT_SECRET, {
    expiresIn: "1h",
  });
}

export function verifyToken(token: string): AuthToken {
  invariant(process.env.JWT_SECRET, "JWT_SECRET env var not set.");

  try {
    const payload = verify(token, process.env.JWT_SECRET) as JwtPayload;

    if (payload["type"] !== "register" && payload["type"] !== "login") {
      console.error("Token is not a register nor a login");
      throw json({ error: "Invalid token" });
    }

    if (!payload.email) {
      console.error("token without an email");
      throw json({ error: "Invalid token" });
    }

    return payload as AuthToken;
  } catch (e) {
    console.error(e);
    throw json({ error: "Invalid token" }, 401);
  }
}
