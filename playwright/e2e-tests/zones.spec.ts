import { test, expect } from "@playwright/test";

import { goToApp } from "../utils/navigation";
import { addZone } from "../utils/zones";
import { addCrop } from "../utils/calendar-actions";

test("Adding and deleting zones", async ({ page }) => {
  await goToApp(page, "/plan");

  await page.getByRole("link", { name: "Garden map" }).click();
  await addZone(page, "Zone A", "zone description");
  await addZone(page, "Zone B", "Optional text describing the zone");

  await page
    .getByRole("listitem")
    .filter({ hasText: "Zone A" })
    .getByRole("button", { name: "delete" })
    .click();

  await expect(page.getByRole("listitem").filter({ hasText: "Zone A" })).not.toBeVisible();

  await addZone(page, "Zone C", "");

  await page.getByRole("link", { name: "Crops" }).click();

  await addCrop(page, "Tomatoes");
  await expect(page.getByRole("heading", { name: "Zone B" })).toBeVisible();
  await expect(page.getByRole("heading", { name: "Zone C" })).toBeVisible();
});
