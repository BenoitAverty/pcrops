import { test, expect } from "@playwright/test";

import { goToApp } from "../utils/navigation";

test.beforeEach(async () => {
  await fetch(
    `http://${process.env.MAILHOG_HOST}:${process.env.MAILHOG_API_PORT}/api/v1/messages`,
    {
      method: "DELETE",
    },
  );
});

test("Register an anonymous user", async ({ page }) => {
  await goToApp(page, "/register");
  await page.getByPlaceholder("Your email address").click();
  await page.getByPlaceholder("Your email address").fill("pcrops@pcrops.test");
  await page.getByRole("button", { name: "Register" }).click();

  await expect(page.getByText("Thanks")).toBeVisible();
  await expect(
    page.getByText(
      "An email has been sent to pcrops@pcrops.test. Click the link inside to finish the registration process.",
    ),
  ).toBeVisible();

  const response = await fetch(
    `http://${process.env.MAILHOG_HOST}:${process.env.MAILHOG_API_PORT}/api/v2/messages`,
  );
  const mails = await response.json();
  expect(mails.count).toEqual(1);
  expect(mails.items[0].To[0].Mailbox).toEqual("pcrops");
  expect(mails.items[0].To[0].Domain).toEqual("pcrops.test");
  expect(mails.items[0].Content.Headers.Subject[0]).toEqual(
    "Confirm your registration to pcrops.fly.dev",
  );

  const body = mails.items[0].Content.Body.replace(/=\r\n/g, "");
  expect(body).toMatch(/Confirm registration at http:\/\/(.*)\/magic\/.+/);
  const regexResult = body.match(/Confirm registration at (http:\/\/(.*)\/magic\/.+)/)[1];

  await page.goto(regexResult);
  await expect(page).toHaveURL(/\/plan\/crops$/);
  await expect(page.getByTestId("unregistered-alert")).not.toBeVisible();
});
