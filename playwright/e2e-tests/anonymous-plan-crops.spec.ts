import { test, expect } from "@playwright/test";

import { goToApp } from "../utils/navigation";
import { addCrop, addCropSowTransplant } from "../utils/calendar-actions";

test("Basic crop operations", async ({ page }) => {
  await goToApp(page, "/plan/crops");
  await expect(page.getByPlaceholder("species name...")).toBeFocused();
  await addCropSowTransplant(page, "Tomatoes");
  await addCrop(page, "Corn");

  // Moving planting week
  const initialPlantingWeek = page
    .getByRole("row", { name: "Tomatoes" })
    .getByTestId("week-2023-15-middle");
  const targetPlantingWeek = page
    .getByRole("row", { name: "Tomatoes" })
    .getByTestId("week-2023-19");
  await initialPlantingWeek.dragTo(targetPlantingWeek);

  // Resizing harvest
  const initialHarvestEnd = page.getByRole("row", { name: "Tomatoes" }).getByTestId("week-2023-35");
  const targetHarvestEnd = page.getByRole("row", { name: "Tomatoes" }).getByTestId("week-2023-43");
  await initialHarvestEnd.dragTo(targetHarvestEnd);

  await expect(page).toHaveScreenshot({ fullPage: true });
});

test("Cascading actions", async ({ page }) => {
  await goToApp(page, "/plan/crops");
  await addCrop(page, "Concombres");
  await page.getByTestId("week-2023-32").dragTo(page.getByTestId("week-2023-35"));
  await page.getByTestId("week-2023-28").dragTo(page.getByTestId("week-2023-38"));

  await expect(page).toHaveScreenshot({ fullPage: true });
});

test("Delete crops", async ({ page }) => {
  await goToApp(page, "/plan/crops");

  await addCrop(page, "Tomates");
  await addCrop(page, "Concombres");

  const deleteButton = page
    .getByRole("row", { name: "Tomates" })
    .getByRole("button", { name: "Delete crop" });

  await expect(deleteButton).not.toBeVisible();
  await page.getByRole("row", { name: "Tomates" }).locator("th").hover();
  await expect(deleteButton).toBeVisible();
  await deleteButton.click();
  await expect(page.getByRole("row", { name: "Tomates" })).not.toBeVisible();
  await expect(page.getByRole("row", { name: "Concombres" })).toBeVisible();
});
