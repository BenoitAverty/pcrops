import util from "util";
import { exec } from "child_process";
import * as path from "path";

import { DockerComposeEnvironment, Wait } from "testcontainers";
import { fetch } from "@remix-run/node";

const execPromise = util.promisify(exec);

export default async function globalSetup() {
  let testLocalAppStatus;
  try {
    testLocalAppStatus = (await fetch("http://localhost:3000")).status;
  } catch (e) {
    testLocalAppStatus;
  }
  if (testLocalAppStatus == 200) {
    console.debug(
      "Global setup : something is running on http://localhost:3000. Assuming the app is already started.\n\n\n",
    );
    process.env.MAILHOG_HOST = "localhost";
    process.env.MAILHOG_API_PORT = `8025`;
    process.env.APP_URL = `http://localhost:3000`;
  } else {
    console.debug("Global setup : starting containers");
    const composeEnvironment = await new DockerComposeEnvironment(
      path.resolve("__dirname", "..", "scripts"),
      "dev-docker-compose.yml",
    )
      .withNoRecreate()
      .withProfiles("app")
      .withWaitStrategy(
        "postgres",
        Wait.forLogMessage(/database system is ready to accept connections/),
      )
      .up();
    console.debug(`Global setup : containers started`);

    // TODO stream container logs to a file.

    const postgresUri = `${composeEnvironment
      .getContainer("postgres")
      .getHost()}:${composeEnvironment.getContainer("postgres").getMappedPort(5432)}`;
    process.env.DATABASE_URL = `postgresql://postgres:mysecretpassword@${postgresUri}/postgres?schema=public`;

    console.debug("Global setup : starting database schema migration");
    await execPromise("./node_modules/.bin/prisma migrate deploy");
    console.debug("Global setup : database schema migration finished");

    const smtpHost = composeEnvironment.getContainer("mailhog").getHost();
    const mailhogApiPort = composeEnvironment.getContainer("mailhog").getMappedPort(8025);

    process.env.MAILHOG_HOST = smtpHost;
    process.env.MAILHOG_API_PORT = `${mailhogApiPort}`;
    const appHost = composeEnvironment.getContainer("app").getHost();
    const appPort = composeEnvironment.getContainer("app").getMappedPort(3000);
    process.env.APP_URL = `http://${appHost}:${appPort}`;

    console.debug("\n\n\n");

    return async () => {
      console.debug("Global teardown : stop containers");
      await composeEnvironment.down({ timeout: 10000, removeVolumes: true });
    };
  }
}
