import { Page } from "playwright-core";
import { expect } from "@playwright/test";

export async function addCrop(page: Page, crop: string) {
  await page.getByPlaceholder("species name...").fill(crop);
  await page.getByPlaceholder("species name...").press("Enter");
  await expect(page.getByRole("row", { name: crop })).toBeVisible();
}
export async function addCropSowTransplant(page: Page, crop: string) {
  await page.getByPlaceholder("species name...").fill(crop);
  await page.getByRole("button", { name: "Sow and transplant" }).click();
  await expect(page.getByRole("row", { name: crop })).toBeVisible();
}
