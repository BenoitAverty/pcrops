import { Page } from "playwright-core";
import { expect } from "@playwright/test";

export async function addZone(page: Page, zoneName: string, zoneDescription: string) {
  await page.getByPlaceholder("Name").fill(zoneName);
  await page.getByPlaceholder("Name").press("Tab");
  await page.getByPlaceholder("Description").fill(zoneDescription);
  await page.getByPlaceholder("Description").press("Enter");

  await expect(
    page.getByRole("listitem").filter({ has: page.getByRole("heading", { name: zoneName }) }),
  ).toContainText(zoneDescription);
}
