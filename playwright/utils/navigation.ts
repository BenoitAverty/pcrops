import { Page } from "playwright-core";

/**
 * like page.goto but prepends the app url.
 * Can't use the config parameter because the app url is not known before global setup.
 */
export function goToApp(page: Page, location: string, options = {}) {
  return page.goto(process.env.APP_URL + location, options);
}
