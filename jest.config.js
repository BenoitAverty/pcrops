// eslint-disable-next-line no-undef
module.exports = {
  transform: {
    "^.+\\.tsx?$": "jest-esbuild",
  },
  testPathIgnorePatterns: ["playwright"],
};
