const plugin = require("tailwindcss/plugin");

module.exports = {
  // not putting "mode: jit" here because it makes the Idea plugin break. Instead,
  // the --jit option is given in the command line.
  content: ["./app/**/*.{ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [
    plugin(function ({ addVariant }) {
      // Add a `third` variant, ie. `third:pb-0`
      addVariant("second", "&:nth-child(2)");
    }),
  ],
};
