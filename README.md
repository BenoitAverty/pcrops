# A Remix template

This is a remix template (as in "A repo that you can fork") that contains my preferred development tools and deployment option for a remix app.

## Deployment

Fly.io

## Development

### Gitlab CI

### Eslint + prettier

### Tailwindcss